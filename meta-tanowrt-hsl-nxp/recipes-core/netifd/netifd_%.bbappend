#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020-2021 Tano Systems LLC. All rights reserved.
#
PR:append:ls1028ardb = ".nxp4"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files:"
