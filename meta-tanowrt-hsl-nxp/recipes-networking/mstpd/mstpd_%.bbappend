#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Tano Systems LLC. All rights reserved.
#
PR:append = ".nxp0"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files:"
