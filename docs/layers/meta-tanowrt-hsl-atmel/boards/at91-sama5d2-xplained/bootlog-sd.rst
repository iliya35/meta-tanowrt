.. SPDX-License-Identifier: MIT

SAMA5D2 Xplained Booting from External SD Card Logs
===================================================

AT91Bootstrap
-------------

.. tabs::

   .. tab:: 4.0.5

      .. literalinclude :: logs/bootlog-sd-at91bootstrap-4.0.5.txt
         :language: text

   .. tab:: 3.10.1 (Unsupported)

      .. literalinclude :: logs/bootlog-sd-at91bootstrap-3.10.1.txt
         :language: text

U-Boot
------

.. tabs::

   .. tab:: 2020.01

      .. literalinclude :: logs/bootlog-sd-u-boot-2020.01.txt
         :language: text

Linux Kernel (OS)
-----------------

.. note::

   This is the first power-on logs after writing an image to the SD card,
   so there is a step of automatically resizong (expanding) the overlay
   partition to the entire SD card.

.. tabs::

   .. tab:: 6.1.74

      .. literalinclude :: logs/bootlog-sd-kernel-6.1.74.txt
         :language: text

   .. tab:: 4.19.78

      .. literalinclude :: logs/bootlog-sd-kernel-4.19.78.txt
         :language: text
