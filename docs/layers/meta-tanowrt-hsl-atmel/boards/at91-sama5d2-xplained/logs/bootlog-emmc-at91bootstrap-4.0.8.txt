AT91Bootstrap 4.0.8-gitAUTOINC+c331a16179-r0.tano3.2 (2023-12-18 09:57:32)

EEPROM: Loading AT24xx information ...
EEPROM: BoardName | [Revid] | VendorName
  #0  SAMA5D2-XULT [AB1]      ATMEL-RF0
EEPROM: BoardDate | Year | Week
EEPROM:             2016    22

EEPROM: Board sn: 0xd300000 revision: 0x400000

SD/MMC: Image: Read file u-boot.bin to 0x26f00000
MMC: ADMA supported
MMC: Specification Version 4.0 or higher
MMC: v5.0/5.01 detected
MMC: highspeed supported
MMC: Dual Data Rate supported
MMC: detecting buswidth...
MMC: 8-bit bus width detected
SD/MMC: Done to load image
<debug_uart>
