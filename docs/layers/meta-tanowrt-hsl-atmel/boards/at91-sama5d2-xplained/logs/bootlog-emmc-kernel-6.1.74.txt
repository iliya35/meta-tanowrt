[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.1.20.0.1 (oe-user@oe-host) (arm-oe-linux-gnueabi-gcc (GCC) 11.4.0, GNU ld (GNU Binutils) 2.38.20220708) #1 Tue Jan 23 16:07:43 UTC 2024
[    0.000000] CPU: ARMv7 Processor [410fc051] revision 1 (ARMv7), cr=10c53c7d
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
[    0.000000] OF: fdt: Machine model: Atmel SAMA5D2 Xplained
[    0.000000] Memory policy: Data cache writeback
[    0.000000] cma: Reserved 16 MiB at 0x3e800000
[    0.000000] Zone ranges:
[    0.000000]   Normal   [mem 0x0000000020000000-0x000000003fffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000020000000-0x000000003fffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000020000000-0x000000003fffffff]
[    0.000000] CPU: All CPU(s) started in SVC mode.
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 129920
[    0.000000] Kernel command line: console=ttyS0,115200n8  root=/dev/mmcblk0p3 ro rootfstype=squashfs rootwait rootfs_partition=3 rootfs_volume=3 earlyprintk panic=15
[    0.000000] Unknown kernel command line parameters "earlyprintk rootfs_partition=3 rootfs_volume=3", will be passed to user space.
[    0.000000] Dentry cache hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    0.000000] Inode-cache hash table entries: 32768 (order: 5, 131072 bytes, linear)
[    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.000000] Memory: 488740K/524288K available (9216K kernel code, 461K rwdata, 2336K rodata, 1024K init, 168K bss, 19164K reserved, 16384K cma-reserved)
[    0.000000] NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
[    0.000000] L2C-310 ID prefetch enabled, offset 2 lines
[    0.000000] L2C-310 dynamic clock gating enabled, standby mode enabled
[    0.000000] L2C-310 cache controller enabled, 8 ways, 128 kB
[    0.000000] L2C-310: CACHE_ID 0x410000c9, AUX_CTRL 0x36020000
[    0.000000] clocksource: timer@f800c000: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 184217874325 ns
[    0.000003] sched_clock: 32 bits at 10MHz, resolution 96ns, wraps every 206986376143ns
[    0.000042] Switching to timer-based delay loop, resolution 96ns
[    0.000304] clocksource: pit: mask: 0x7ffffff max_cycles: 0x7ffffff, max_idle_ns: 11513617062 ns
[    0.001110] Console: colour dummy device 80x30
[    0.001209] Calibrating delay loop (skipped), value calculated using timer frequency.. 20.75 BogoMIPS (lpj=103750)
[    0.001245] CPU: Testing write buffer coherency: ok
[    0.001349] pid_max: default: 32768 minimum: 301
[    0.001774] Mount-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.001823] Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.005426] Setting up static identity map for 0x20100000 - 0x20100060
[    0.006911] devtmpfs: initialized
[    0.020995] VFP support v0.3: implementor 41 architecture 2 part 30 variant 5 rev 1
[    0.021534] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
[    0.021587] futex hash table entries: 256 (order: -1, 3072 bytes, linear)
[    0.023375] pinctrl core: initialized pinctrl subsystem
[    0.026514] NET: Registered PF_NETLINK/PF_ROUTE protocol family
[    0.029215] DMA: preallocated 256 KiB pool for atomic coherent allocations
[    0.031947] cpuidle: using governor menu
[    0.059143] AT91: PM: standby: standby, suspend: ulp0
[    0.082682] at_xdmac f0010000.dma-controller: 16 channels, mapped at 0x(ptrval)
[    0.086131] at_xdmac f0004000.dma-controller: 16 channels, mapped at 0x(ptrval)
[    0.087747] AT91: Detected SoC family: sama5d2
[    0.087770] AT91: Detected SoC: sama5d27, revision 1
[    0.089054] SCSI subsystem initialized
[    0.089707] usbcore: registered new interface driver usbfs
[    0.089848] usbcore: registered new interface driver hub
[    0.089964] usbcore: registered new device driver usb
[    0.091245] mc: Linux media interface: v0.10
[    0.091410] videodev: Linux video capture interface: v2.00
[    0.091561] pps_core: LinuxPPS API ver. 1 registered
[    0.091576] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.091627] PTP clock support registered
[    0.092259] Advanced Linux Sound Architecture Driver Initialized.
[    0.093530] Bluetooth: Core ver 2.22
[    0.093689] NET: Registered PF_BLUETOOTH protocol family
[    0.093704] Bluetooth: HCI device and connection manager initialized
[    0.093735] Bluetooth: HCI socket layer initialized
[    0.093753] Bluetooth: L2CAP socket layer initialized
[    0.093796] Bluetooth: SCO socket layer initialized
[    0.095315] clocksource: Switched to clocksource timer@f800c000
[    0.122625] NET: Registered PF_INET protocol family
[    0.123210] IP idents hash table entries: 8192 (order: 4, 65536 bytes, linear)
[    0.126109] tcp_listen_portaddr_hash hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.126199] Table-perturb hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    0.126252] TCP established hash table entries: 4096 (order: 2, 16384 bytes, linear)
[    0.126349] TCP bind hash table entries: 4096 (order: 3, 32768 bytes, linear)
[    0.126507] TCP: Hash tables configured (established 4096 bind 4096)
[    0.126765] UDP hash table entries: 256 (order: 0, 4096 bytes, linear)
[    0.126826] UDP-Lite hash table entries: 256 (order: 0, 4096 bytes, linear)
[    0.127240] NET: Registered PF_UNIX/PF_LOCAL protocol family
[    0.128556] RPC: Registered named UNIX socket transport module.
[    0.128590] RPC: Registered udp transport module.
[    0.128599] RPC: Registered tcp transport module.
[    0.128606] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.130096] Initialise system trusted keyrings
[    0.131020] workingset: timestamp_bits=14 max_order=17 bucket_order=3
[    0.131991] squashfs: version 4.0 (2009/01/31) Phillip Lougher
[    0.345538] Key type asymmetric registered
[    0.345576] Asymmetric key parser 'x509' registered
[    0.345727] io scheduler mq-deadline registered
[    0.345752] io scheduler kyber registered
[    0.358762] pinctrl-at91-pio4 fc038000.pinctrl: atmel pinctrl initialized
[    0.381240] brd: module loaded
[    0.397527] loop: module loaded
[    0.399292] atmel_usart_serial.0.auto: ttyS0 at MMIO 0xf8020000 (irq = 152, base_baud = 5187500) is a ATMEL_SERIAL
[    0.975760] printk: console [ttyS0] enabled
[    0.982611] atmel_usart_serial.1.auto: ttyS1 at MMIO 0xfc008000 (irq = 153, base_baud = 5187500) is a ATMEL_SERIAL
[    0.996968] at91_i2c fc018600.i2c: can't get DMA channel, continue without DMA support
[    1.004854] at91_i2c fc018600.i2c: Using FIFO (16 data)
[    1.010539] i2c i2c-3: using pinctrl states for GPIO recovery
[    1.016426] i2c i2c-3: using generic GPIOs for recovery
[    1.021761] at91_i2c fc018600.i2c: AT91 i2c bus driver (hw version: 0x704).
[    1.033885] atmel_spi f8000000.spi: Using dma0chan0 (tx) and dma0chan1 (rx) for DMA transfers
[    1.042586] atmel_spi f8000000.spi: Using FIFO (16 data)
[    1.050243] spi-nor spi0.0: at25df321a (4096 Kbytes)
[    1.059082] atmel_spi f8000000.spi: Atmel SPI Controller version 0x311 at 0xf8000000 (irq 155)
[    1.071125] CAN device driver interface
[    1.082138] macb f8008000.ethernet eth0: Cadence GEM rev 0x00020203 at 0xf8008000 irq 156 (fc:c2:3d:02:f4:39)
[    1.093108] PPP generic driver version 2.4.2
[    1.098516] PPP MPPE Compression module registered
[    1.103277] NET: Registered PF_PPPOX protocol family
[    1.109005] atmel-ehci 500000.ehci: EHCI Host Controller
[    1.114337] atmel-ehci 500000.ehci: new USB bus registered, assigned bus number 1
[    1.122446] atmel-ehci 500000.ehci: irq 159, io mem 0x00500000
[    1.155395] atmel-ehci 500000.ehci: USB 2.0 started, EHCI 1.00
[    1.161569] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 6.01
[    1.169852] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.177042] usb usb1: Product: EHCI Host Controller
[    1.181872] usb usb1: Manufacturer: Linux 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.1.20.0.1 ehci_hcd
[    1.191439] usb usb1: SerialNumber: 500000.ehci
[    1.197597] hub 1-0:1.0: USB hub found
[    1.201403] hub 1-0:1.0: 3 ports detected
[    1.208685] at91_ohci 400000.ohci: USB Host Controller
[    1.213858] at91_ohci 400000.ohci: new USB bus registered, assigned bus number 2
[    1.221851] at91_ohci 400000.ohci: irq 159, io mem 0x00400000
[    1.299720] usb usb2: New USB device found, idVendor=1d6b, idProduct=0001, bcdDevice= 6.01
[    1.308008] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.315168] usb usb2: Product: USB Host Controller
[    1.319968] usb usb2: Manufacturer: Linux 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.1.20.0.1 ohci_hcd
[    1.329503] usb usb2: SerialNumber: at91
[    1.334879] hub 2-0:1.0: USB hub found
[    1.338786] hub 2-0:1.0: 3 ports detected
[    1.345094] usbcore: registered new interface driver cdc_acm
[    1.350807] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
[    1.359035] usbcore: registered new interface driver usb-storage
[    1.365231] usbcore: registered new interface driver usbserial_generic
[    1.371845] usbserial: USB Serial support registered for generic
[    1.377923] usbcore: registered new interface driver ftdi_sio
[    1.383677] usbserial: USB Serial support registered for FTDI USB Serial Device
[    1.394144] at91_rtc f80480b0.rtc: registered as rtc0
[    1.399302] at91_rtc f80480b0.rtc: setting system clock to 2012-01-01T00:00:09 UTC (1325376009)
[    1.408035] at91_rtc f80480b0.rtc: AT91 Real Time Clock driver.
[    1.414265] i2c_dev: i2c /dev entries driver
[    1.420340] at91-reset f8048000.reset-controller: Starting after wakeup
[    1.430004] sama5d4_wdt f8048040.watchdog: initialized (timeout = 16 sec, nowayout = 0)
[    1.438818] Bluetooth: HCI UART driver ver 2.3
[    1.443217] Bluetooth: HCI UART protocol H4 registered
[    1.448531] Bluetooth: HCI UART protocol Three-wire (H5) registered
[    1.454898] usbcore: registered new interface driver btusb
[    1.461065] sdhci: Secure Digital Host Controller Interface driver
[    1.467249] sdhci: Copyright(c) Pierre Ossman
[    1.471780] sdhci-pltfm: SDHCI platform and OF driver helper
[    1.479644] ledtrig-cpu: registered to indicate activity on CPUs
[    1.486335] atmel_aes f002c000.crypto: version: 0x500
[    1.495042] atmel_aes f002c000.crypto: Atmel AES - Using dma0chan2, dma0chan3 for DMA transfers
[    1.504844] atmel_sha f0028000.crypto: version: 0x510
[    1.512043] atmel_sha f0028000.crypto: using dma0chan4 for DMA transfers
[    1.519108] atmel_sha f0028000.crypto: Atmel SHA1/SHA256/SHA224/SHA384/SHA512
[    1.527442] atmel_tdes fc044000.crypto: version: 0x703
[    1.533392] atmel_tdes fc044000.crypto: using dma0chan5, dma0chan6 for DMA transfers
[    1.541507] atmel_tdes fc044000.crypto: Atmel DES/TDES
[    1.547379] usbcore: registered new interface driver usbhid
[    1.552905] usbhid: USB HID core driver
[    1.562832] atmel-classd fc048000.classd: PWM modulation type is Differential, non-overlapping is enabled
[    1.573009] mmc0: SDHCI controller on a0000000.sdio-host [a0000000.sdio-host] using ADMA
[    1.584502] u32 classifier
[    1.587234]     input device check on
[    1.590829]     Actions configured
[    1.594886] NET: Registered PF_INET6 protocol family
[    1.603141] Segment Routing with IPv6
[    1.607028] In-situ OAM (IOAM) with IPv6
[    1.611089] sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
[    1.618640] NET: Registered PF_PACKET protocol family
[    1.623800] Bridge firewalling registered
[    1.627852] can: controller area network core
[    1.632350] NET: Registered PF_CAN protocol family
[    1.637162] can: raw protocol
[    1.640078] can: broadcast manager protocol
[    1.644245] can: netlink gateway - max_hops=1
[    1.649569] Bluetooth: RFCOMM TTY layer initialized
[    1.654495] Bluetooth: RFCOMM socket layer initialized
[    1.659685] Bluetooth: RFCOMM ver 1.11
[    1.663432] Bluetooth: BNEP (Ethernet Emulation) ver 1.3
[    1.668728] Bluetooth: BNEP filters: protocol multicast
[    1.673910] Bluetooth: BNEP socket layer initialized
[    1.678874] Bluetooth: HIDP (Human Interface Emulation) ver 1.2
[    1.684747] Bluetooth: HIDP socket layer initialized
[    1.689770] l2tp_core: L2TP core driver, V2.0
[    1.694082] l2tp_ppp: PPPoL2TP kernel driver, V2.0
[    1.698863] l2tp_netlink: L2TP netlink interface
[    1.704741] Loading compiled-in X.509 certificates
[    1.752054] at91_i2c f8028000.i2c: can't get DMA channel, continue without DMA support
[    1.760058] at91_i2c f8028000.i2c: Using FIFO (16 data)
[    1.766185] i2c i2c-0: using pinctrl states for GPIO recovery
[    1.772075] i2c i2c-0: using generic GPIOs for recovery
[    1.800200] mmc0: new DDR MMC card at address 0001
[    1.807848] mmcblk0: mmc0:0001 Q2J54A 3.59 GiB
[    1.820598]  mmcblk0: p1 p2 p3 p4 < p5 p6 p7 >
[    1.829528] at91_i2c f8028000.i2c: AT91 i2c bus driver (hw version: 0x704).
[    1.837677] at91_i2c fc028000.i2c: can't get DMA channel, continue without DMA support
[    1.845656] at91_i2c fc028000.i2c: Using FIFO (16 data)
[    1.853580] mmcblk0boot0: mmc0:0001 Q2J54A 16.0 MiB
[    1.862721] mmcblk0boot1: mmc0:0001 Q2J54A 16.0 MiB
[    1.871198] mmcblk0rpmb: mmc0:0001 Q2J54A 512 KiB, chardev (247:0)
[    1.877893] i2c i2c-1: using pinctrl states for GPIO recovery
[    1.883726] i2c i2c-1: using generic GPIOs for recovery
[    1.889756] at24 1-0054: supply vcc not found, using dummy regulator
[    1.897727] at24 1-0054: 256 byte 24c02 EEPROM, writable, 16 bytes/write
[    1.904582] at91_i2c fc028000.i2c: AT91 i2c bus driver (hw version: 0x704).
[    1.921298] at91-sama5d2_adc fc030000.adc: setting up trigger as external_rising
[    1.928785] at91-sama5d2_adc fc030000.adc: version: 800
[    1.936781] input: gpio-keys as /devices/platform/gpio-keys/input/input0
[    1.946243] ALSA device list:
[    1.949179]   #0: CLASSD
[    1.952544] atmel_usart_serial atmel_usart_serial.0.auto: using dma0chan8 for rx DMA transfers
[    1.962119] atmel_usart_serial atmel_usart_serial.0.auto: using dma0chan9 for tx DMA transfers
[    2.006370] mmc1: SDHCI controller on b0000000.sdio-host [b0000000.sdio-host] using ADMA
[    2.027629] VFS: Mounted root (squashfs filesystem) readonly on device 179:3.
[    2.037249] devtmpfs: mounted
[    2.043130] Freeing unused kernel image (initmem) memory: 1024K
[    2.049355] Run /sbin/init as init process
[    2.186891] init: Console is alive
[    2.190830] init: - watchdog -
[    3.176923] kmodloader: loading kernel modules from /etc/modules-boot.d/*
[    3.209557] kmodloader: done loading kernel modules from /etc/modules-boot.d/*
[    3.227828] init: - preinit -
[    6.255361] random: crng init done
Press the [f] key and hit [enter] to enter failsafe mode
Press the [1], [2], [3] or [4] key and hit [enter] to select the debug level
[    9.854461] SWUPDATE: AT91Bootstrap version 4.0.5-gitAUTOINC+8fe4b67188-r0.tano3.2
[   10.007062] SWUPDATE: U-Boot version 2020.01-gitAUTOINC+af59b26c22-tano0.3
[   10.115888] SWUPDATE: U-Boot startup version 1.0.0-tano1.atmel1
[   10.145901] SWUPDATE: Kernel version 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.1.20.0.1
[   10.184215] SWUPDATE: Read-only filesystem version 2024-04-04-21-31-45-UTC
[   10.376128] mount_root: /dev/mmcblk0p1: p1, rw, start 1048576, size 8388608
[   10.387513] mount_root: /dev/mmcblk0p2: p2, rw, start 9437184, size 134217728
[   10.395456] mount_root: /dev/mmcblk0p3: p3, rw, start 143654912, size 1073741824 [rootfs]
[   10.405082] mount_root: /dev/mmcblk0p4: p4, rw, start 1217396736, size 1024
[   10.416364] mount_root: /dev/mmcblk0p5: p5, rw, start 1218445312, size 134217728
[   10.425766] mount_root: /dev/mmcblk0p6: p6, rw, start 1353711616, size 1073741824
[   10.437487] mount_root: /dev/mmcblk0p7: p7, rw, start 2428502016, size 1421869056 [overlay]
[   10.446175] mount_root: root filesystem on the /dev/mmcblk0p3 partition of /dev/mmcblk0 (rw) device
[   10.471679] mount_root: founded suitable overlay partition /dev/mmcblk0p7
[   10.482817] mount_root: loading kmods from internal overlay
[   10.809340] kmodloader: loading kernel modules from //etc/modules-boot.d/*
[   10.829193] kmodloader: done loading kernel modules from //etc/modules-boot.d/*
[   11.027625] EXT4-fs (mmcblk0p7): mounted filesystem with ordered data mode. Quota mode: disabled.
[   11.037242] block: attempting to load /tmp/ext4_cfg/upper/etc/config/fstab
[   11.045900] block: unable to load configuration (fstab: Entry not found)
[   11.052777] block: attempting to load /tmp/ext4_cfg/etc/config/fstab
[   11.059555] block: unable to load configuration (fstab: Entry not found)
[   11.066544] block: attempting to load /etc/config/fstab
[   11.081774] block: extroot: not configured
[   11.087954] EXT4-fs (mmcblk0p7): unmounting filesystem.
[   11.109267] EXT4-fs (mmcblk0p7): mounted filesystem with ordered data mode. Quota mode: disabled.
[   11.242090] block: attempting to load /tmp/ext4_cfg/upper/etc/config/fstab
[   11.249526] block: unable to load configuration (fstab: Entry not found)
[   11.256535] block: attempting to load /tmp/ext4_cfg/etc/config/fstab
[   11.263147] block: unable to load configuration (fstab: Entry not found)
[   11.270144] block: attempting to load /etc/config/fstab
[   11.276475] block: extroot: not configured
[   11.283681] mount_root: overlay filesystem has not been fully initialized yet
[   11.296962] mount_root: switching to ext4 overlay
[   11.509028] Root filesystem mounted
[   13.637452] urandom-seed: Seed file not found (/etc/urandom.seed)
[   13.702008] procd: - watchdog -
[   13.715692] procd: - ubus -
[   13.801771] procd: - init -
Please press Enter to activate this console.
[   17.461078] kmodloader: loading kernel modules from /etc/modules.d/*
[   17.517394] tun: Universal TUN/TAP device driver, 1.6
[   17.744619] atmel_usba_udc 300000.gadget: MMIO registers at [mem 0xfc02c000-0xfc02c3ff] mapped at d851d58e
[   17.754470] atmel_usba_udc 300000.gadget: FIFO at [mem 0x00300000-0x003fffff] mapped at 0853c96e
[   17.805803] usbcore: registered new interface driver cdc_wdm
[   17.897473] cfg80211: Loading compiled-in X.509 certificates for regulatory database
[   18.150667] cfg80211: Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
[   18.169600] cfg80211: Loaded X.509 cert 'wens: 61c038651aabdcf94bd0ac7ff06c7248db18c600'
[   18.178162] platform regulatory.0: Direct firmware load for regulatory.db failed with error -2
[   18.186824] cfg80211: failed to load regulatory.db
[   18.217059] cryptodev: loading out-of-tree module taints kernel.
[   18.246173] cryptodev: driver 1.12 loaded.
[   18.260513] gre: GRE over IPv4 demultiplexor driver
[   18.488081] PPTP driver version 0.8.5
[   18.639876] xt_time: kernel timezone is +0400
[   18.659780] usbcore: registered new interface driver cdc_ether
[   18.690540] usbcore: registered new interface driver cdc_ncm
[   18.793137] usbcore: registered new interface driver qmi_wwan
[   18.888591] usbcore: registered new interface driver cdc_mbim
[   18.928709] kmodloader: done loading kernel modules from /etc/modules.d/*
[   30.777994] Cgroup memory moving (move_charge_at_immigrate) is deprecated. Please report your usecase to linux-mm@kvack.org if you depend on this functionality.
[   34.488913] udevd[1367]: starting version 3.2.10
[   34.668005] udevd[1367]: starting eudev-3.2.10
[   67.915712] macb f8008000.ethernet eth0: PHY [f8008000.ethernet-ffffffff:01] driver [Micrel KSZ8081 or KSZ8091] (irq=97)
[   67.955632] macb f8008000.ethernet eth0: configuring for phy/rmii link mode
[   67.993401] br-lan: port 1(eth0) entered blocking state
[   67.998689] br-lan: port 1(eth0) entered disabled state
[   68.004399] device eth0 entered promiscuous mode
[   70.311658] macb f8008000.ethernet eth0: Link is Up - 100Mbps/Full - flow control off
[   70.386724] br-lan: port 1(eth0) entered blocking state
[   70.391941] br-lan: port 1(eth0) entered forwarding state
[   70.441280] IPv6: ADDRCONF(NETDEV_CHANGE): br-lan: link becomes ready
[   95.918492] using random self ethernet address
[   95.922916] using random host ethernet address
[   95.929105] usb0: HOST MAC 52:07:a0:26:16:6d
[   95.933353] usb0: MAC 12:87:7d:65:e1:ef
[   95.937334] using random self ethernet address
[   95.941718] using random host ethernet address
[   95.946319] g_ether gadget.0: Ethernet Gadget, version: Memorial Day 2008
[   95.953054] g_ether gadget.0: g_ether ready
[   96.132405] IPv6: ADDRCONF(NETDEV_CHANGE): usb0: link becomes ready
