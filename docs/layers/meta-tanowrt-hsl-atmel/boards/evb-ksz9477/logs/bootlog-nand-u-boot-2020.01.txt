U-Boot 2020.01-gitAUTOINC+af59b26c22-tano0.3 (Jan 15 2021 - 07:11:03 +0000)

CPU: SAMA5D36
Crystal frequency:       12 MHz
CPU clock        :      528 MHz
Master clock     :      132 MHz
DRAM:  256 MiB
WDT:   Started with servicing (15s timeout)
NAND:  256 MiB
MMC:   Atmel mci: 0, Atmel mci: 1
Loading Environment from NAND... OK
In:    serial@ffffee00
Out:   serial@ffffee00
Err:   serial@ffffee00
Net:
Warning: ethernet@f0028000 MAC addresses don't match:
Address in ROM is          00:10:a1:98:97:02
Address in environment is  00:10:a1:98:97:01
eth0: ethernet@f0028000
Hit any key to stop autoboot:  0

NAND read: device 0 offset 0x180000, size 0x40000
 262144 bytes read: OK
## Executing script at 21000000
Board name: sama5d3_xplained
Active system A
Loading kernel fitImage...
ubi0: attaching mtd5
ubi0: scanning is finished
ubi0: attached mtd5 (name "system_a", size 104 MiB)
ubi0: PEB size: 131072 bytes (128 KiB), LEB size: 126976 bytes
ubi0: min./max. I/O unit sizes: 2048/2048, sub-page size 2048
ubi0: VID header offset: 2048 (aligned 2048), data offset: 4096
ubi0: good PEBs: 832, bad PEBs: 0, corrupted PEBs: 0
ubi0: user volume: 2, internal volumes: 1, max. volumes count: 128
ubi0: max/mean erase counter: 27/14, WL threshold: 4096, image sequence number: 1194712431
ubi0: available PEBs: 539, total reserved PEBs: 293, PEBs reserved for bad PEB handling: 40
No size specified -> Using max size (6348800)
Read 6348800 bytes from volume kernel to 24000000
Booting kernel 0x24000000#conf-at91-evb-ksz9477.dtb...
## Loading kernel from FIT Image at 24000000 ...
   Using 'conf-at91-evb-ksz9477.dtb' configuration
   Trying 'kernel-1' kernel subimage
     Description:  Linux kernel
     Type:         Kernel Image
     Compression:  uncompressed
     Data Start:   0x24000110
     Data Size:    6148552 Bytes = 5.9 MiB
     Architecture: ARM
     OS:           Linux
     Load Address: 0x22000000
     Entry Point:  0x22000000
     Hash algo:    sha256
     Hash value:   35c61ee1fc6bfb8075a64b942748fd784606a4f0811404fd0cb67a2725216ee1
   Verifying Hash Integrity ... sha256+ OK
## Loading fdt from FIT Image at 24000000 ...
   Using 'conf-at91-evb-ksz9477.dtb' configuration
   Trying 'fdt-at91-evb-ksz9477.dtb' fdt subimage
     Description:  Flattened Device Tree blob
     Type:         Flat Device Tree
     Compression:  uncompressed
     Data Start:   0x245dd3ec
     Data Size:    40674 Bytes = 39.7 KiB
     Architecture: ARM
     Load Address: 0x23e00000
     Hash algo:    sha256
     Hash value:   0c6d7b5e399d6f517c27e2c6b4e493a0516448595cc808fe7a395121cd5babdc
   Verifying Hash Integrity ... sha256+ OK
   Loading fdt from 0x245dd3ec to 0x23e00000
   Booting using the fdt blob at 0x23e00000
   Loading Kernel Image
   Loading Device Tree to 2fb20000, end 2fb2cee1 ... OK

Starting kernel ...
