[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.0.20.0.2 (oe-user@oe-host) (arm-oe-linux-gnueabi-gcc (GCC) 11.4.0, GNU ld (GNU Binutils) 2.38.20220708) #1 Tue Jan 23 16:07:43 UTC 2024
[    0.000000] CPU: ARMv7 Processor [410fc051] revision 1 (ARMv7), cr=10c53c7d
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
[    0.000000] OF: fdt: Machine model: Microchip EVB-KSZ9477
[    0.000000] Memory policy: Data cache writeback
[    0.000000] cma: Reserved 16 MiB at 0x2e800000
[    0.000000] Zone ranges:
[    0.000000]   Normal   [mem 0x0000000020000000-0x000000002fffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000020000000-0x000000002fffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000020000000-0x000000002fffffff]
[    0.000000] CPU: All CPU(s) started in SVC mode.
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 64960
[    0.000000] Kernel command line: console=ttyS0,115200n8  ubi.mtd=system_a,2048 ubi.mtd=rootfs_data,2048 ubi.block=0,rootfs root=/dev/ubiblock0_1 ro rootfstype=squashfs rootwait rootfs_partition=system_a rootfs_volume=rootfs earlyprintk panic=15
[    0.000000] Unknown kernel command line parameters "earlyprintk rootfs_partition=system_a rootfs_volume=rootfs", will be passed to user space.
[    0.000000] Dentry cache hash table entries: 32768 (order: 5, 131072 bytes, linear)
[    0.000000] Inode-cache hash table entries: 16384 (order: 4, 65536 bytes, linear)
[    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.000000] Memory: 229076K/262144K available (9216K kernel code, 463K rwdata, 2384K rodata, 1024K init, 166K bss, 16684K reserved, 16384K cma-reserved)
[    0.000000] NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
[    0.000000] clocksource: pit: mask: 0xfffffff max_cycles: 0xfffffff, max_idle_ns: 14479245754 ns
[    0.000000] Console: colour dummy device 80x30
[    0.000000] sched_clock: 32 bits at 100 Hz, resolution 10000000ns, wraps every 21474836475000000ns
[    0.000000] Calibrating delay loop... 351.43 BogoMIPS (lpj=1757184)
[    0.060000] CPU: Testing write buffer coherency: ok
[    0.060000] pid_max: default: 32768 minimum: 301
[    0.060000] Mount-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.060000] Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.060000] Setting up static identity map for 0x20100000 - 0x20100060
[    0.060000] devtmpfs: initialized
[    0.070000] VFP support v0.3: implementor 41 architecture 2 part 30 variant 5 rev 1
[    0.070000] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
[    0.070000] futex hash table entries: 256 (order: -1, 3072 bytes, linear)
[    0.080000] pinctrl core: initialized pinctrl subsystem
[    0.080000] NET: Registered PF_NETLINK/PF_ROUTE protocol family
[    0.080000] DMA: preallocated 256 KiB pool for atomic coherent allocations
[    0.080000] cpuidle: using governor menu
[    0.120000] AT91: PM: standby: standby, suspend: ulp0
[    0.120000] gpio-at91 fffff200.gpio: at address (ptrval)
[    0.120000] gpio-at91 fffff400.gpio: at address (ptrval)
[    0.120000] gpio-at91 fffff600.gpio: at address (ptrval)
[    0.120000] gpio-at91 fffff800.gpio: at address (ptrval)
[    0.130000] gpio-at91 fffffa00.gpio: at address (ptrval)
[    0.130000] pinctrl-at91 ahb:apb:pinctrl@fffff200: initialized AT91 pinctrl driver
[    0.150000] at_hdmac ffffe600.dma-controller: Atmel AHB DMA Controller ( cpy set slave ), 8 channels
[    0.150000] at_hdmac ffffe800.dma-controller: Atmel AHB DMA Controller ( cpy set slave ), 8 channels
[    0.160000] AT91: Detected SoC family: sama5d3
[    0.160000] AT91: Detected SoC: sama5d36, revision 2
[    0.160000] SCSI subsystem initialized
[    0.160000] usbcore: registered new interface driver usbfs
[    0.160000] usbcore: registered new interface driver hub
[    0.160000] usbcore: registered new device driver usb
[    0.160000] at91_i2c f0014000.i2c: using dma0chan0 (tx) and dma0chan1 (rx) for DMA transfers
[    0.160000] i2c i2c-0: using pinctrl states for GPIO recovery
[    0.160000] i2c i2c-0: using generic GPIOs for recovery
[    0.160000] at91_i2c f0014000.i2c: AT91 i2c bus driver (hw version: 0x402).
[    0.160000] at91_i2c f0018000.i2c: using dma0chan2 (tx) and dma0chan3 (rx) for DMA transfers
[    0.160000] i2c i2c-1: using pinctrl states for GPIO recovery
[    0.160000] i2c i2c-1: using generic GPIOs for recovery
[    0.160000] at91_i2c f0018000.i2c: AT91 i2c bus driver (hw version: 0x402).
[    0.160000] at91_i2c f801c000.i2c: can't get DMA channel, continue without DMA support
[    0.170000] i2c i2c-2: using pinctrl states for GPIO recovery
[    0.170000] i2c i2c-2: using generic GPIOs for recovery
[    0.170000] at91_i2c f801c000.i2c: AT91 i2c bus driver (hw version: 0x402).
[    0.170000] mc: Linux media interface: v0.10
[    0.170000] videodev: Linux video capture interface: v2.00
[    0.170000] pps_core: LinuxPPS API ver. 1 registered
[    0.170000] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.170000] PTP clock support registered
[    0.170000] Advanced Linux Sound Architecture Driver Initialized.
[    0.170000] Bluetooth: Core ver 2.22
[    0.170000] NET: Registered PF_BLUETOOTH protocol family
[    0.170000] Bluetooth: HCI device and connection manager initialized
[    0.170000] Bluetooth: HCI socket layer initialized
[    0.170000] Bluetooth: L2CAP socket layer initialized
[    0.170000] Bluetooth: SCO socket layer initialized
[    0.170000] clocksource: Switched to clocksource pit
[    0.200000] NET: Registered PF_INET protocol family
[    0.200000] IP idents hash table entries: 4096 (order: 3, 32768 bytes, linear)
[    0.210000] tcp_listen_portaddr_hash hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.210000] Table-perturb hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    0.210000] TCP established hash table entries: 2048 (order: 1, 8192 bytes, linear)
[    0.210000] TCP bind hash table entries: 2048 (order: 2, 16384 bytes, linear)
[    0.210000] TCP: Hash tables configured (established 2048 bind 2048)
[    0.210000] UDP hash table entries: 256 (order: 0, 4096 bytes, linear)
[    0.210000] UDP-Lite hash table entries: 256 (order: 0, 4096 bytes, linear)
[    0.210000] NET: Registered PF_UNIX/PF_LOCAL protocol family
[    0.210000] RPC: Registered named UNIX socket transport module.
[    0.210000] RPC: Registered udp transport module.
[    0.210000] RPC: Registered tcp transport module.
[    0.210000] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.210000] Initialise system trusted keyrings
[    0.210000] workingset: timestamp_bits=14 max_order=16 bucket_order=2
[    0.210000] squashfs: version 4.0 (2009/01/31) Phillip Lougher
[    0.420000] Key type asymmetric registered
[    0.420000] Asymmetric key parser 'x509' registered
[    0.420000] io scheduler mq-deadline registered
[    0.420000] io scheduler kyber registered
[    0.440000] brd: module loaded
[    0.460000] loop: module loaded
[    0.460000] atmel_usart_serial.0.auto: ttyS1 at MMIO 0xf001c000 (irq = 27, base_baud = 4125000) is a ATMEL_SERIAL
[    0.460000] atmel_usart_serial.1.auto: ttyS2 at MMIO 0xf0020000 (irq = 28, base_baud = 4125000) is a ATMEL_SERIAL
[    0.460000] atmel_usart_serial.2.auto: ttyS5 at MMIO 0xf0024000 (irq = 29, base_baud = 4125000) is a ATMEL_SERIAL
[    0.470000] atmel_usart_serial.3.auto: ttyS0 at MMIO 0xffffee00 (irq = 30, base_baud = 8250000) is a ATMEL_SERIAL
[    1.130000] printk: console [ttyS0] enabled
[    1.140000] atmel_spi f0004000.spi: Using dma0chan4 (tx) and dma0chan5 (rx) for DMA transfers
[    1.150000] atmel_spi f0004000.spi: Atmel SPI Controller version 0x213 at 0xf0004000 (irq 31)
[    1.160000] atmel_spi f8008000.spi: Using dma1chan0 (tx) and dma1chan1 (rx) for DMA transfers
[    1.170000] ksz9477@0 enforce active low on chipselect handle
[    1.180000] atmel_spi f8008000.spi: Atmel SPI Controller version 0x213 at 0xf8008000 (irq 32)
[    1.190000] CAN device driver interface
[    1.190000] ksz9477-switch spi1.0: Microchip KSZ switch driver version 0.9.0
[    1.200000] ksz9477-switch spi1.0: Microchip KSZ9477 Switch (7 ports)
[    1.210000] ksz9477-switch spi1.0: Failed to register DSA switch (-517)
[    1.860000] macb f0028000.ethernet eth0: Cadence GEM rev 0x00020119 at 0xf0028000 irq 34 (00:10:a1:98:97:01)
[    1.870000] PPP generic driver version 2.4.2
[    1.870000] PPP MPPE Compression module registered
[    1.880000] NET: Registered PF_PPPOX protocol family
[    1.890000] atmel-ehci 700000.ehci: EHCI Host Controller
[    1.890000] atmel-ehci 700000.ehci: new USB bus registered, assigned bus number 1
[    1.900000] atmel-ehci 700000.ehci: irq 35, io mem 0x00700000
[    1.940000] atmel-ehci 700000.ehci: USB 2.0 started, EHCI 1.00
[    1.940000] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 6.01
[    1.950000] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.960000] usb usb1: Product: EHCI Host Controller
[    1.960000] usb usb1: Manufacturer: Linux 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.0.20.0.2 ehci_hcd
[    1.970000] usb usb1: SerialNumber: 700000.ehci
[    1.980000] hub 1-0:1.0: USB hub found
[    1.980000] hub 1-0:1.0: 3 ports detected
[    1.990000] at91_ohci 600000.ohci: USB Host Controller
[    1.990000] at91_ohci 600000.ohci: new USB bus registered, assigned bus number 2
[    2.000000] at91_ohci 600000.ohci: irq 35, io mem 0x00600000
[    2.080000] usb usb2: New USB device found, idVendor=1d6b, idProduct=0001, bcdDevice= 6.01
[    2.090000] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    2.090000] usb usb2: Product: USB Host Controller
[    2.100000] usb usb2: Manufacturer: Linux 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.0.20.0.2 ohci_hcd
[    2.110000] usb usb2: SerialNumber: at91
[    2.110000] hub 2-0:1.0: USB hub found
[    2.120000] hub 2-0:1.0: 3 ports detected
[    2.120000] usbcore: registered new interface driver cdc_acm
[    2.130000] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
[    2.140000] usbcore: registered new interface driver usb-storage
[    2.150000] usbcore: registered new interface driver usbserial_generic
[    2.150000] usbserial: USB Serial support registered for generic
[    2.160000] usbcore: registered new interface driver ftdi_sio
[    2.160000] usbserial: USB Serial support registered for FTDI USB Serial Device
[    2.170000] at91_rtc fffffeb0.rtc: registered as rtc0
[    2.180000] at91_rtc fffffeb0.rtc: setting system clock to 2024-04-04T11:33:02 UTC (1712230382)
[    2.190000] at91_rtc fffffeb0.rtc: AT91 Real Time Clock driver.
[    2.190000] i2c_dev: i2c /dev entries driver
[    2.200000] at91-reset fffffe00.reset-controller: Starting after wakeup
[    2.210000] at91_wdt fffffe40.watchdog: watchdog already configured differently (mr = 1fff2eff expecting 1fff2fff)
[    2.220000] at91sam9_wdt: enabled (heartbeat=15 sec, nowayout=0)
[    2.230000] Bluetooth: HCI UART driver ver 2.3
[    2.230000] Bluetooth: HCI UART protocol H4 registered
[    2.240000] Bluetooth: HCI UART protocol Three-wire (H5) registered
[    2.240000] usbcore: registered new interface driver btusb
[    2.250000] sdhci: Secure Digital Host Controller Interface driver
[    2.260000] sdhci: Copyright(c) Pierre Ossman
[    2.260000] sdhci-pltfm: SDHCI platform and OF driver helper
[    2.270000] ledtrig-cpu: registered to indicate activity on CPUs
[    2.270000] atmel_aes f8038000.crypto: version: 0x135
[    2.280000] atmel_aes f8038000.crypto: Atmel AES - Using dma1chan2, dma1chan3 for DMA transfers
[    2.290000] atmel_sha f8034000.crypto: version: 0x410
[    2.290000] atmel_sha f8034000.crypto: using dma1chan4 for DMA transfers
[    2.300000] atmel_sha f8034000.crypto: Atmel SHA1/SHA256/SHA224/SHA384/SHA512
[    2.310000] atmel_tdes f803c000.crypto: version: 0x701
[    2.310000] atmel_tdes f803c000.crypto: using dma1chan5, dma1chan6 for DMA transfers
[    2.320000] atmel_tdes f803c000.crypto: Atmel DES/TDES
[    2.330000] usbcore: registered new interface driver usbhid
[    2.330000] usbhid: USB HID core driver
[    2.340000] nand: device found, Manufacturer ID: 0x2c, Chip ID: 0xda
[    2.350000] nand: Micron MT29F2G08ABAEAWP
[    2.350000] nand: 256 MiB, SLC, erase size: 128 KiB, page size: 2048, OOB size: 64
[    2.360000] atmel_mci f0000000.mmc: version: 0x505
[    2.370000] atmel_mci f0000000.mmc: using dma0chan7 for DMA transfers
[    2.380000] atmel_mci f8000000.mmc: version: 0x505
[    2.380000] atmel_mci f8000000.mmc: using dma1chan7 for DMA transfers
[    2.390000] Bad block table found at page 131008, version 0x01
[    2.390000] Bad block table found at page 130944, version 0x01
[    2.400000] atmel_mci f0000000.mmc: Atmel MCI controller at 0xf0000000 irq 42, 1 slots
[    2.410000] 7 fixed-partitions partitions found on MTD device atmel_nand
[    2.420000] Creating 7 MTD partitions on "atmel_nand":
[    2.420000] 0x000000000000-0x000000040000 : "at91bootstrap"
[    2.430000] atmel_mci f8000000.mmc: Atmel MCI controller at 0xf8000000 irq 43, 1 slots
[    2.440000] 0x000000040000-0x000000180000 : "uboot"
[    2.450000] 0x000000180000-0x0000001c0000 : "startup"
[    2.460000] 0x0000001c0000-0x000000200000 : "ubootenv"
[    2.470000] 0x000000200000-0x000006a00000 : "system_a"
[    2.470000] 0x000006a00000-0x00000d200000 : "system_b"
[    2.480000] 0x00000d200000-0x000010000000 : "rootfs_data"
[    2.490000] iio iio:device0: Resolution used: 12 bits
[    2.500000] iio iio:device0: ADC Touch screen is disabled.
[    2.510000] u32 classifier
[    2.510000]     input device check on
[    2.520000]     Actions configured
[    2.520000] NET: Registered PF_INET6 protocol family
[    2.530000] Segment Routing with IPv6
[    2.530000] In-situ OAM (IOAM) with IPv6
[    2.540000] sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
[    2.550000] NET: Registered PF_PACKET protocol family
[    2.550000] Bridge firewalling registered
[    2.560000] can: controller area network core
[    2.560000] NET: Registered PF_CAN protocol family
[    2.570000] can: raw protocol
[    2.570000] can: broadcast manager protocol
[    2.570000] can: netlink gateway - max_hops=1
[    2.580000] Bluetooth: RFCOMM TTY layer initialized
[    2.580000] Bluetooth: RFCOMM socket layer initialized
[    2.590000] Bluetooth: RFCOMM ver 1.11
[    2.590000] Bluetooth: BNEP (Ethernet Emulation) ver 1.3
[    2.600000] Bluetooth: BNEP filters: protocol multicast
[    2.600000] Bluetooth: BNEP socket layer initialized
[    2.610000] Bluetooth: HIDP (Human Interface Emulation) ver 1.2
[    2.610000] Bluetooth: HIDP socket layer initialized
[    2.620000] l2tp_core: L2TP core driver, V2.0
[    2.620000] l2tp_ppp: PPPoL2TP kernel driver, V2.0
[    2.630000] l2tp_netlink: L2TP netlink interface
[    2.630000] Loading compiled-in X.509 certificates
[    2.670000] ksz9477-switch spi1.0: Microchip KSZ switch driver version 0.9.0
[    2.680000] ksz9477-switch spi1.0: Microchip KSZ9477 Switch (7 ports)
[    2.690000] ksz9477-switch spi1.0: Use rgmii-txid
[    2.700000] ksz9477-switch spi1.0: Port 5: Enabled tail tagging
[    2.720000] ksz9477-switch spi1.0: Enabled jumbo frames support
[    2.720000] ksz9477-switch spi1.0: The switch has been successfully started
[    2.740000] ksz9477-switch spi1.0: Using legacy PHYLIB callbacks. Please migrate to PHYLINK!
[    2.750000] ksz9477-switch spi1.0 sw1p1 (uninitialized): PHY [dsa-0.0:00] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    2.770000] ksz9477-switch spi1.0 sw1p2 (uninitialized): PHY [dsa-0.0:01] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    2.780000] ksz9477-switch spi1.0 sw1p3 (uninitialized): PHY [dsa-0.0:02] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    2.790000] ksz9477-switch spi1.0 sw1p4 (uninitialized): PHY [dsa-0.0:03] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    2.810000] ksz9477-switch spi1.0 sw1p5 (uninitialized): PHY [dsa-0.0:04] driver [Microchip KSZ9477 Switch] (irq=POLL)
[    2.820000] device eth0 entered promiscuous mode
[    2.830000] DSA: tree 0 setup
[    2.830000] ksz9477-switch spi1.0: User ports mask is 0x5f
[    2.830000] ksz9477-switch spi1.0: CPU port MAC: 00:10:a1:98:97:01
[    2.840000] ksz9477-switch spi1.0: Port 0 [sw1p1] MAC: 00:10:a1:98:97:02
[    2.850000] ksz9477-switch spi1.0: Port 1 [sw1p2] MAC: 00:10:a1:98:97:03
[    2.850000] ksz9477-switch spi1.0: Port 2 [sw1p3] MAC: 00:10:a1:98:97:04
[    2.860000] ksz9477-switch spi1.0: Port 3 [sw1p4] MAC: 00:10:a1:98:97:05
[    2.870000] ksz9477-switch spi1.0: Port 4 [sw1p5] MAC: 00:10:a1:98:97:06
[    2.880000] ksz9477-switch spi1.0: Port 6 [sw1p6] MAC: 00:10:a1:98:97:07
[    2.880000] ksz9477-switch spi1.0: Port mask: 0x7f, CPU mask: 0x20, DSA mask: 0x0, user mask: 0x5f
[    2.890000] ksz9477-switch spi1.0: Port 0 [USER]: sw1p1
[    2.900000] ksz9477-switch spi1.0: Port 1 [USER]: sw1p2
[    2.900000] ksz9477-switch spi1.0: Port 2 [USER]: sw1p3
[    2.910000] ksz9477-switch spi1.0: Port 3 [USER]: sw1p4
[    2.910000] ksz9477-switch spi1.0: Port 4 [USER]: sw1p5
[    2.920000] ksz9477-switch spi1.0: Port 5 [CPU]: eth0
[    2.920000] ksz9477-switch spi1.0: Port 6 [USER]: sw1p6
[    2.930000] ksz9477-switch spi1.0: DSA switch registered
[    2.930000] ksz9477-switch spi1.0: Created sysfs entries
[    2.940000] ubi0: default fastmap pool size: 40
[    2.940000] ubi0: default fastmap WL pool size: 20
[    2.950000] ubi0: attaching mtd4
[    3.450000] ubi0: scanning is finished
[    3.460000] ubi0: attached mtd4 (name "system_a", size 104 MiB)
[    3.470000] ubi0: PEB size: 131072 bytes (128 KiB), LEB size: 126976 bytes
[    3.480000] ubi0: min./max. I/O unit sizes: 2048/2048, sub-page size 2048
[    3.480000] ubi0: VID header offset: 2048 (aligned 2048), data offset: 4096
[    3.490000] ubi0: good PEBs: 832, bad PEBs: 0, corrupted PEBs: 0
[    3.500000] ubi0: user volume: 2, internal volumes: 1, max. volumes count: 128
[    3.500000] ubi0: max/mean erase counter: 27/14, WL threshold: 4096, image sequence number: 1194712431
[    3.510000] ubi0: available PEBs: 537, total reserved PEBs: 295, PEBs reserved for bad PEB handling: 40
[    3.520000] ubi0: background thread "ubi_bgt0d" started, PID 100
[    3.530000] ubi1: default fastmap pool size: 15
[    3.530000] ubi1: default fastmap WL pool size: 7
[    3.540000] ubi1: attaching mtd6
[    3.780000] ubi1: scanning is finished
[    3.790000] ubi1: attached mtd6 (name "rootfs_data", size 46 MiB)
[    3.800000] ubi1: PEB size: 131072 bytes (128 KiB), LEB size: 126976 bytes
[    3.810000] ubi1: min./max. I/O unit sizes: 2048/2048, sub-page size 2048
[    3.810000] ubi1: VID header offset: 2048 (aligned 2048), data offset: 4096
[    3.820000] ubi1: good PEBs: 364, bad PEBs: 4, corrupted PEBs: 0
[    3.830000] ubi1: user volume: 1, internal volumes: 1, max. volumes count: 128
[    3.830000] ubi1: max/mean erase counter: 31/16, WL threshold: 4096, image sequence number: 40562245
[    3.840000] ubi1: available PEBs: 0, total reserved PEBs: 364, PEBs reserved for bad PEB handling: 36
[    3.850000] ubi1: background thread "ubi_bgt1d" started, PID 101
[    3.860000] block ubiblock0_1: created from ubi0:1(rootfs)
[    3.870000] ALSA device list:
[    3.870000]   No soundcards found.
[    3.880000] VFS: Mounted root (squashfs filesystem) readonly on device 254:0.
[    3.890000] devtmpfs: mounted
[    3.890000] Freeing unused kernel image (initmem) memory: 1024K
[    3.900000] Run /sbin/init as init process
[    4.120000] init: Console is alive
[    4.120000] init: - watchdog -
[    5.150000] random: crng init done
[    5.480000] kmodloader: loading kernel modules from /etc/modules-boot.d/*
[    5.510000] kmodloader: done loading kernel modules from /etc/modules-boot.d/*
[    5.540000] init: - preinit -
Press the [f] key and hit [enter] to enter failsafe mode
Press the [1], [2], [3] or [4] key and hit [enter] to select the debug level
[    9.960000] SWUPDATE: AT91Bootstrap version 4.0.5-gitAUTOINC+8fe4b67188-r0.tano3.2
[   10.340000] SWUPDATE: U-Boot version 2020.01-gitAUTOINC+af59b26c22-tano0.3
[   10.460000] SWUPDATE: U-Boot startup version 1.0.0-tano1.atmel1
[   10.500000] SWUPDATE: Kernel version 6.1.74-tano-mchp-standard-g3f74b634cc-tano0.2.0.20.0.2
[   10.530000] SWUPDATE: Read-only filesystem version 2024-04-04-08-26-34-UTC
[   10.800000] mount_root: loading kmods from internal overlay
[   11.170000] kmodloader: loading kernel modules from //etc/modules-boot.d/*
[   11.180000] kmodloader: done loading kernel modules from //etc/modules-boot.d/*
[   11.330000] UBIFS (ubi1:0): default file-system created
[   11.330000] UBIFS (ubi1:0): Mounting in unauthenticated mode
[   11.340000] UBIFS (ubi1:0): background thread "ubifs_bgt1_0" started, PID 187
[   11.430000] UBIFS (ubi1:0): UBIFS: mounted UBI device 1, volume 0, name "rootfs_data"
[   11.440000] UBIFS (ubi1:0): LEB size: 126976 bytes (124 KiB), min./max. I/O unit sizes: 2048 bytes/2048 bytes
[   11.450000] UBIFS (ubi1:0): FS size: 39616512 bytes (37 MiB, 312 LEBs), max 322 LEBs, journal size 2031616 bytes (1 MiB, 16 LEBs)
[   11.460000] UBIFS (ubi1:0): reserved for root: 1871185 bytes (1827 KiB)
[   11.460000] UBIFS (ubi1:0): media format: w5/r0 (latest is w5/r0), UUID A5149243-9568-4239-AFCF-A89B56166F28, small LPT model
[   11.470000] block: attempting to load /tmp/ubifs_cfg/upper/etc/config/fstab
[   11.470000] block: unable to load configuration (fstab: Entry not found)
[   11.480000] block: attempting to load /tmp/ubifs_cfg/etc/config/fstab
[   11.490000] block: unable to load configuration (fstab: Entry not found)
[   11.490000] block: attempting to load /etc/config/fstab
[   11.510000] block: extroot: not configured
[   11.520000] UBIFS (ubi1:0): un-mount UBI device 1
[   11.530000] UBIFS (ubi1:0): background thread "ubifs_bgt1_0" stops
[   11.540000] UBIFS (ubi1:0): Mounting in unauthenticated mode
[   11.540000] UBIFS (ubi1:0): background thread "ubifs_bgt1_0" started, PID 188
[   11.630000] UBIFS (ubi1:0): UBIFS: mounted UBI device 1, volume 0, name "rootfs_data"
[   11.640000] UBIFS (ubi1:0): LEB size: 126976 bytes (124 KiB), min./max. I/O unit sizes: 2048 bytes/2048 bytes
[   11.650000] UBIFS (ubi1:0): FS size: 39616512 bytes (37 MiB, 312 LEBs), max 322 LEBs, journal size 2031616 bytes (1 MiB, 16 LEBs)
[   11.660000] UBIFS (ubi1:0): reserved for root: 1871185 bytes (1827 KiB)
[   11.670000] UBIFS (ubi1:0): media format: w5/r0 (latest is w5/r0), UUID A5149243-9568-4239-AFCF-A89B56166F28, small LPT model
[   11.770000] block: attempting to load /tmp/ubifs_cfg/upper/etc/config/fstab
[   11.770000] block: unable to load configuration (fstab: Entry not found)
[   11.780000] block: attempting to load /tmp/ubifs_cfg/etc/config/fstab
[   11.790000] block: unable to load configuration (fstab: Entry not found)
[   11.790000] block: attempting to load /etc/config/fstab
[   11.800000] block: extroot: not configured
[   11.810000] mount_root: overlay filesystem has not been fully initialized yet
[   11.820000] mount_root: switching to ubifs overlay
[   12.040000] Root filesystem mounted
[   14.530000] urandom-seed: Seed file not found (/etc/urandom.seed)
[   14.610000] procd: - watchdog -
[   14.630000] procd: - ubus -
[   14.730000] procd: - init -
Please press Enter to activate this console.
[   18.750000] kmodloader: loading kernel modules from /etc/modules.d/*
[   18.800000] tun: Universal TUN/TAP device driver, 1.6
[   19.010000] atmel_usba_udc 500000.gadget: MMIO registers at [mem 0xf8030000-0xf8033fff] mapped at 7cbdddcf
[   19.020000] atmel_usba_udc 500000.gadget: FIFO at [mem 0x00500000-0x005fffff] mapped at 90c2ba9d
[   19.050000] usbcore: registered new interface driver cdc_wdm
[   19.120000] cfg80211: Loading compiled-in X.509 certificates for regulatory database
[   19.410000] cfg80211: Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
[   19.420000] cfg80211: Loaded X.509 cert 'wens: 61c038651aabdcf94bd0ac7ff06c7248db18c600'
[   19.430000] platform regulatory.0: Direct firmware load for regulatory.db failed with error -2
[   19.440000] cfg80211: failed to load regulatory.db
[   19.450000] cryptodev: loading out-of-tree module taints kernel.
[   19.480000] cryptodev: driver 1.12 loaded.
[   19.500000] gre: GRE over IPv4 demultiplexor driver
[   19.660000] PPTP driver version 0.8.5
[   19.790000] xt_time: kernel timezone is +0300
[   19.800000] usbcore: registered new interface driver cdc_ether
[   19.820000] usbcore: registered new interface driver cdc_ncm
[   19.900000] usbcore: registered new interface driver qmi_wwan
[   19.970000] usbcore: registered new interface driver cdc_mbim
[   20.010000] kmodloader: done loading kernel modules from /etc/modules.d/*
[   32.690000] Cgroup memory moving (move_charge_at_immigrate) is deprecated. Please report your usecase to linux-mm@kvack.org if you depend on this functionality.
[   37.230000] udevd[1374]: starting version 3.2.10
[   37.530000] udevd[1374]: starting eudev-3.2.10
[   79.710000] macb f0028000.ethernet eth0: configuring for fixed/rgmii link mode
[   79.720000] macb f0028000.ethernet eth0: Link is Up - 1Gbps/Full - flow control off
[   79.720000] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[   79.820000] ksz9477-switch spi1.0 sw1p1: configuring for phy/gmii link mode
[   79.860000] ksz9477-switch spi1.0 sw1p1: Link is Up - 1Gbps/Full - flow control off
[   79.870000] br-lan: port 1(sw1p1) entered blocking state
[   79.870000] br-lan: port 1(sw1p1) entered disabled state
[   79.920000] device sw1p1 entered promiscuous mode
[   79.960000] br-lan: port 1(sw1p1) entered blocking state
[   80.100000] ksz9477-switch spi1.0 sw1p2: configuring for phy/gmii link mode
[   80.140000] br-lan: port 2(sw1p2) entered blocking state
[   80.140000] br-lan: port 2(sw1p2) entered disabled state
[   80.250000] device sw1p2 entered promiscuous mode
[   80.380000] ksz9477-switch spi1.0 sw1p3: configuring for phy/gmii link mode
[   80.420000] br-lan: port 3(sw1p3) entered blocking state
[   80.420000] br-lan: port 3(sw1p3) entered disabled state
[   80.520000] device sw1p3 entered promiscuous mode
[   80.610000] ksz9477-switch spi1.0 sw1p4: configuring for phy/gmii link mode
[   80.660000] br-lan: port 4(sw1p4) entered blocking state
[   80.660000] br-lan: port 4(sw1p4) entered disabled state
[   80.700000] device sw1p4 entered promiscuous mode
[   80.800000] ksz9477-switch spi1.0 sw1p6: configuring for fixed/ link mode
[   80.810000] ksz9477-switch spi1.0 sw1p6: Link is Up - 1Gbps/Full - flow control off
[   80.850000] br-lan: port 5(sw1p6) entered blocking state
[   80.860000] br-lan: port 5(sw1p6) entered disabled state
[   80.880000] ksz9477-switch spi1.0 sw1p1: Link is Down
[   80.910000] device sw1p6 entered promiscuous mode
[   80.920000] br-lan: port 5(sw1p6) entered blocking state
[   80.960000] br-lan: port 1(sw1p1) entered disabled state
[   81.090000] ksz9477-switch spi1.0 sw1p5: configuring for phy/gmii link mode
[   84.000000] ksz9477-switch spi1.0 sw1p1: Link is Up - 1Gbps/Full - flow control off
[   84.000000] br-lan: port 1(sw1p1) entered blocking state
[   87.700000] br-lan: port 1(sw1p1) entered blocking state
[   87.790000] br-lan: port 5(sw1p6) entered blocking state
[   88.870000] br-lan: port 1(sw1p1) entered learning state
[   88.880000] br-lan: port 1(sw1p1) entered forwarding state
[   88.880000] IPv6: ADDRCONF(NETDEV_CHANGE): br-lan: link becomes ready
[   89.830000] br-lan: port 5(sw1p6) entered learning state
[   89.830000] br-lan: port 5(sw1p6) entered forwarding state
[  119.620000] using random self ethernet address
[  119.620000] using random host ethernet address
[  119.630000] usb0: HOST MAC 26:50:14:39:a2:e0
[  119.630000] usb0: MAC 5e:be:9f:13:94:2b
[  119.630000] using random self ethernet address
[  119.640000] using random host ethernet address
[  119.640000] g_ether gadget.0: Ethernet Gadget, version: Memorial Day 2008
[  119.650000] g_ether gadget.0: g_ether ready


tanowrt login: root
Password:
 _______            __          __   _
|__   __|           \ \        / /  | |    Embedded Linux Distribution
   | | __ _ _ __   __\ \  /\  / / __| |_   by Tano Systems
   | |/ _` | '_ \ / _ \ \/  \/ / '__| __|
   | | (_| | | | | (_) \  /\  /| |  | |_   (c) 2018-2024 Tano Systems LLC
   |_|\__,_|_| |_|\___/ \/  \/ |_|   \__|  https://tano-systems.com

   Board:    Microchip EVB-KSZ9477 (NAND)
   Release:  Kirkstone
   Revision: c584e770702714a974a197294fe4849559ac995c
             2024-04-04 08:26:34 UTC

[root@tanowrt ~]#
