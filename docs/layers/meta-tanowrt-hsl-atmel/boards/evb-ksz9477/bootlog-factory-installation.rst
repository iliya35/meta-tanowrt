.. SPDX-License-Identifier: MIT

Microchip EVB-KSZ9477 Factory Installation to NAND Flash Logs
=============================================================

Here is the complete debug output for booting and running
from the SD card the image for initial factory system
installation to the internal NAND flash memory.

.. tabs::

   .. tab:: AT91Bootstrap 4.0.8, U-Boot 2023.07, Kernel 6.1.74

      .. literalinclude :: logs/bootlog-factory-installation-a4.0.8-u2023.07-k6.1.74.txt
         :language: text

   .. tab:: AT91Bootstrap 4.0.5, U-Boot 2020.01, Kernel 6.1.74

      .. literalinclude :: logs/bootlog-factory-installation-a4.0.5-u2020.01-k6.1.74.txt
         :language: text
