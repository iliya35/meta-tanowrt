.. SPDX-License-Identifier: MIT

SAMA5D3 Xplained Booting from Internal NAND Flash Logs
======================================================

AT91Bootstrap
-------------

.. tabs::

   .. tab:: 4.0.8

      .. literalinclude :: logs/bootlog-nand-at91bootstrap-4.0.8.txt
         :language: text

   .. tab:: 4.0.5

      .. literalinclude :: logs/bootlog-nand-at91bootstrap-4.0.5.txt
         :language: text

   .. tab:: 3.10.1 (Unsupported)

      .. literalinclude :: logs/bootlog-nand-at91bootstrap-3.10.1.txt
         :language: text

U-Boot
------

.. tabs::

   .. tab:: 2023.01

      .. literalinclude :: logs/bootlog-nand-u-boot-2023.07.txt
         :language: text

   .. tab:: 2020.01

      .. literalinclude :: logs/bootlog-nand-u-boot-2020.01.txt
         :language: text

Linux Kernel (OS)
-----------------

.. tabs::

   .. tab:: 6.1.74

      .. literalinclude :: logs/bootlog-nand-kernel-6.1.74.txt
         :language: text

   .. tab:: 4.19.78

      .. literalinclude :: logs/bootlog-nand-kernel-4.19.78.txt
         :language: text

