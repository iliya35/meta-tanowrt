.. SPDX-License-Identifier: MIT

FriendlyElec NanoPi R5C Booting from External SD Card Logs
==========================================================

DDR Initialization
------------------

.. tabs::

   .. tab:: 1.13

      .. literalinclude :: logs/bootlog-sd-ddr-1.13.txt
         :language: text

U-Boot
------

.. tabs::

   .. tab:: 2017.09

      .. literalinclude :: logs/bootlog-sd-u-boot-2017.09.txt
         :language: text

Linux Kernel (OS)
-----------------

.. note::

   This is the first power-on logs after writing an image to the SD card,
   so there is a step of automatically resizong (expanding) the overlay
   partition to the entire SD card.

.. tabs::

   .. tab:: 6.1.57

      .. literalinclude :: logs/bootlog-sd-kernel-6.1.57.txt
         :language: text
