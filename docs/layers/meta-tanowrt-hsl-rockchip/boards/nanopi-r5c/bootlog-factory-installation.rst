.. SPDX-License-Identifier: MIT

FriendlyElec NanoPi R5C Factory Installation to eMMC Logs
=========================================================

Here is the complete debug output for booting and running
from the SD card the image for initial factory system
installation to the internal eMMC flash memory.

.. tabs::

   .. tab:: DDR 1.13, U-Boot 2017.09, Kernel 6.1.57

      .. literalinclude :: logs/bootlog-factory-installation-ddr1.13-u2017.09-k6.1.57.txt
         :language: text
