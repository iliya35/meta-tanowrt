.. SPDX-License-Identifier: MIT

FriendlyElec NanoPi R5C Booting from Internal eMMC Flash Logs
=============================================================

DDR Initialization
------------------

.. tabs::

   .. tab:: 1.13

      .. literalinclude :: logs/bootlog-emmc-ddr-1.13.txt
         :language: text

U-Boot
------

.. tabs::

   .. tab:: 2017.09

      .. literalinclude :: logs/bootlog-emmc-u-boot-2017.09.txt
         :language: text

Linux Kernel (OS)
-----------------

.. tabs::

   .. tab:: 6.1.57

      .. literalinclude :: logs/bootlog-emmc-kernel-6.1.57.txt
         :language: text
