.. table:: Supported Rockchip SoC's Based Boards
   :width: 100%
   :widths: 20, 30, 10, 10, 10, 20

   +---------------------------------------+-----------------------------+--------------------+----------------------------------+--------------------------+------------------------------+
   | Board                                 | SoC                         | RAM                | Supported Kernel Version(s)      | Supported Storage(s)     | Target YAML                  |
   |                                       |                             |                    |                                  |                          | (at :file:`kas/targets`)     |
   +=======================================+=============================+====================+==================================+==========================+==============================+
   | :ref:`machine-boardcon-em3566`        || **Model:** |RK3566-M|      | 2 GiB LPDDR4       || **5.10.198 (non-RT) [Default]** | microSD card             | |boardcon-em3566-sd.yml|     |
   |                                       || **Cores:** |RK3566-C|      |                    || 5.10.110-rt53 (RT)              +--------------------------+------------------------------+
   |                                       || **Frequency:** |RK3566-F|  |                    || 4.19.232 (non-RT)               | 8 GB eMMC\ [#emmc-size]_ | |boardcon-em3566-emmc.yml|   |
   +---------------------------------------+-----------------------------+--------------------+| 4.19.232-rt104 (RT)             +--------------------------+------------------------------+
   | :ref:`machine-boardcon-em3568`        || **Model:** |RK3568-M|      | 2 GiB LPDDR4       |                                  | microSD card             | |boardcon-em3568-sd.yml|     |
   |                                       || **Cores:** |RK3568-C|      |                    |                                  +--------------------------+------------------------------+
   |                                       || **Frequency:** |RK3568-F|  |                    |                                  | 8 GB eMMC\ [#emmc-size]_ | |boardcon-em3568-emmc.yml|   |
   +---------------------------------------+-----------------------------+--------------------+----------------------------------+--------------------------+------------------------------+
   | :ref:`machine-nanopi-r5c`             || **Model:** |RK3568-M|      | up to 4 GiB LPDDR4 || **6.1.57 (non-RT) [Default]**   | microSD card             | |nanopi-r5c-sd.yml|          |
   |                                       || **Cores:** |RK3568-C|      |                    |                                  +--------------------------+------------------------------+
   |                                       || **Frequency:** |RK3568-F|  |                    |                                  | up to 32 GB eMMC         | |nanopi-r5c-emmc.yml|        |
   +---------------------------------------+-----------------------------+--------------------+----------------------------------+--------------------------+------------------------------+
   | :ref:`machine-rock-pi-s`              || **Model:** |RK3308-M|      | up to 512 MiB DDR3 || **5.10.198 (non-RT) [Default]** | microSD card             | |rock-pi-s-sd.yml|           |
   |                                       || **Cores:** |RK3308-C|      |                    || 5.10.110-rt53 (RT)              +--------------------------+------------------------------+
   |                                       || **Frequency:** |RK3308-F|  |                    || 4.19.232 (non-RT)               | SD NAND up to 8 GB       | |rock-pi-s-sdnand.yml|       |
   |                                       |                             |                    || 4.19.232-rt104 (RT)             |                          |                              |
   +---------------------------------------+-----------------------------+--------------------+----------------------------------+--------------------------+------------------------------+

.. |RK3566-M| replace:: Rockchip RK3566
.. |RK3566-C| replace:: 4 x Cortex-A55 ARM 64 bits
.. |RK3566-F| replace:: up to 1.8 GHz

.. |RK3568-M| replace:: Rockchip RK3568
.. |RK3568-C| replace:: 4 x Cortex-A55 ARM 64 bits
.. |RK3568-F| replace:: up to 2.0 GHz

.. |RK3308-M| replace:: Rockchip RK3308
.. |RK3308-C| replace:: 4 x Cortex-A35 ARM 64 bits
.. |RK3308-F| replace:: up to 1.3 GHz

.. |boardcon-em3566-sd.yml|   replace:: :tanowrt_git_blob:`boardcon-em3566-sd.yml   </kas/targets/boardcon-em3566-sd.yml>`
.. |boardcon-em3566-emmc.yml| replace:: :tanowrt_git_blob:`boardcon-em3566-emmc.yml </kas/targets/boardcon-em3566-emmc.yml>`
.. |boardcon-em3568-sd.yml|   replace:: :tanowrt_git_blob:`boardcon-em3568-sd.yml   </kas/targets/boardcon-em3568-sd.yml>`
.. |boardcon-em3568-emmc.yml| replace:: :tanowrt_git_blob:`boardcon-em3568-emmc.yml </kas/targets/boardcon-em3568-emmc.yml>`
.. |nanopi-r5c-sd.yml|        replace:: :tanowrt_git_blob:`nanopi-r5c-sd.yml        </kas/targets/nanopi-r5c-sd.yml>`
.. |nanopi-r5c-emmc.yml|      replace:: :tanowrt_git_blob:`nanopi-r5c-emmc.yml      </kas/targets/nanopi-r5c-emmc.yml>`
.. |rock-pi-s-sd.yml|         replace:: :tanowrt_git_blob:`rock-pi-s-sd.yml         </kas/targets/rock-pi-s-sd.yml>`
.. |rock-pi-s-sdnand.yml|     replace:: :tanowrt_git_blob:`rock-pi-s-sdnand.yml     </kas/targets/rock-pi-s-sdnand.yml>`

.. [#emmc-size] Actual eMMC flash size is approximately 7.28 GiB
