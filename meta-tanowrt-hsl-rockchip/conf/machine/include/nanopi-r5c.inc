# Copyright (c) 2024, Tano Systems LLC
# Released under the MIT license (see COPYING.MIT for the terms)

MACHINEOVERRIDES =. "nanopi-r5c:"

require conf/machine/include/hsl-rk356x.inc

MACHINE_FEATURES:append = "opengl"

# Enable tty1
PROCD_ENABLED_TTYS ?= "1"

#
# Psplash
#
IMAGE_FEATURES += "splash"
SPLASH = "psplash"

# Kernel 6.1
PREFERRED_PROVIDER_virtual/kernel = "linux-tano-rockchip"
PREFERRED_VERSION_linux-tano-rockchip = "6.1%"
LINUXLIBCVERSION = "6.1-custom%"
TANOWRT_WIREGUARD_IN_KERNEL = "1"

MACHINE_EXTRA_RRECOMMENDS:append = " \
	drm-cursor \
"

MACHINE_EXTRA_RDEPENDS:append = " \
	i2c-tools \
	kernel-module-r8125 \
"

# Autoload r8125 kernel module at early boot stage
KERNEL_MODULE_AUTOLOAD:append = " r8125"
KERNEL_MODULE_AUTOLOAD_EARLY:append = " r8125"

# Enable multilib support
require conf/multilib.conf
MULTILIBS = "multilib:lib32"
DEFAULTTUNE:virtclass-multilib-lib32 = "armv7vethf-neon"

RK_WIFIBT_FIRMWARES = " \
	rkwifibt-firmware-rtl8723du-bt \
"

MACHINE_FEATURES += "stp"
MACHINE_FEATURES:remove = "usbgadget"

KERNEL_DEVICETREE = "\
	rockchip/rk3568-nanopi5-rev01.dtb \
	rockchip/rk3568-nanopi5-rev02.dtb \
	rockchip/rk3568-nanopi5-rev03.dtb \
	rockchip/rk3568-nanopi5-rev04.dtb \
	rockchip/rk3568-nanopi5-rev05.dtb \
	rockchip/rk3568-nanopi5-rev07.dtb \
"

RKBIN_DDR_UART_UPDATE = "1"
RKBIN_DDR_UART_ID = "2"
RKBIN_DDR_UART_IOMUX = "0"
RKBIN_DDR_UART_BAUDRATE = "1500000"

RKBIN_DDR_CHIP_NAME = "rk3568"

OPENWRT_VERSION_PRODUCT_EXTRA ?= ""
OPENWRT_VERSION_PRODUCT = "FriendlyElec NanoPi R5C${OPENWRT_VERSION_PRODUCT_EXTRA}"
OPENWRT_VERSION_HWREV = "v1"

# Use hym8563 as default RTC device
OPENWRT_HWCLOCK_DEV = "/dev/rtc1"

TANOWRT_ENABLE_OVERLAY_RESIZE = "1"
