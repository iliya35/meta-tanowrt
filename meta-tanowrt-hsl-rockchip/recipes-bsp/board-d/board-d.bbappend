#
# SPDX-License-Identifier: MIT
# Copyright (c) 2024 Tano Systems LLC. All rights reserved.
#
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files:"

PR:append = ".rk0"

BOARD_D_SCRIPTS:append = "\
	file://01_leds \
	file://02_network \
"
