From b82bd5bc0cbc2a5167875de2ea401082635bd21b Mon Sep 17 00:00:00 2001
From: William Wu <william.wu@rock-chips.com>
Date: Sat, 13 May 2023 14:24:15 +0800
Subject: [PATCH 1015/1015] rockchip: rk3308bs: usb: rockusb: fix abnormal
 linestate

Signed-off-by: William Wu <william.wu@rock-chips.com>
Change-Id: If42aafae9a40af37e86a8f2a3412e0f7ec8dcc37
---
 cmd/rockusb.c                              |  85 ++++++++++++++
 common/usb_hub.c                           |  15 +++
 drivers/phy/phy-rockchip-inno-usb2.c       | 114 ++++++++++++++++++-
 drivers/usb/gadget/dwc2_udc_otg.c          |  14 +++
 drivers/usb/gadget/dwc2_udc_otg_priv.h     |   1 +
 drivers/usb/gadget/dwc2_udc_otg_xfer_dma.c |   3 +
 drivers/usb/gadget/f_mass_storage.c        |   4 +
 drivers/usb/host/ehci-hcd.c                | 126 ++++++++++++++++++++-
 drivers/usb/host/ehci.h                    |   5 +
 include/linux/usb/gadget.h                 |   2 +
 include/linux/usb/phy-rockchip-usb2.h      |  21 ++++
 11 files changed, 387 insertions(+), 3 deletions(-)

diff --git a/cmd/rockusb.c b/cmd/rockusb.c
index 9792a828bb25..d8f27dc5e50e 100644
--- a/cmd/rockusb.c
+++ b/cmd/rockusb.c
@@ -5,6 +5,7 @@
  * SPDX-License-Identifier:	GPL-2.0+
  */
 
+#include <asm/arch/cpu.h>
 #include <errno.h>
 #include <common.h>
 #include <command.h>
@@ -14,6 +15,7 @@
 #include <usb.h>
 #include <usb_mass_storage.h>
 #include <rockusb.h>
+#include <linux/usb/phy-rockchip-usb2.h>
 
 static struct rockusb rkusb;
 static struct rockusb *g_rkusb;
@@ -151,15 +153,25 @@ cleanup:
 	return ret;
 }
 
+#define DM_IS_HIGH		0x2
+#define DP_DM_IS_HIGH		0x3
+#define RETRY_CNT		20
+#define DELAY_MS_WAIT_SE0	5
+#define DELAY_MS_LS_RESUME	10
+#define DELAY_MS_DIS		15
+#define DELAY_MS_POLL		2000
+
 static int do_rkusb(cmd_tbl_t *cmdtp, int flag, int argc, char *const argv[])
 {
 	const char *usb_controller;
 	const char *devtype;
 	const char *devnum;
 	unsigned int controller_index;
+	unsigned int linestate, retry_cnt = RETRY_CNT, wait_conn_delay = 100;
 	int rc;
 	int cable_ready_timeout __maybe_unused;
 	const char *s;
+	bool force_connect = false;
 
 	if (argc != 4)
 		return CMD_RET_USAGE;
@@ -263,11 +275,84 @@ static int do_rkusb(cmd_tbl_t *cmdtp, int flag, int argc, char *const argv[])
 		puts("\r\n");
 	}
 
+force_connect:
+	if (soc_is_rk3308bs()) {
+		/* Wait 10ms for usb controller to complete initialization */
+		mdelay(10);
+
+		while (1) {
+			linestate = rockchip_usb2phy_get_linestate();
+
+			if (!(linestate & DM_IS_HIGH) && !force_connect) {
+				goto connect;
+			}
+
+			retry_cnt = RETRY_CNT;
+			printf("usb linestate 0x%08x error!\n", linestate);
+			rockchip_usb2phy_set_linestate(0, PHY_SET_USB_DP_H_DM_L);
+			mdelay(DELAY_MS_LS_RESUME);
+			rockchip_usb2phy_set_linestate(0, PHY_SET_USB_DISC);
+			mdelay(DELAY_MS_DIS);
+			rockchip_usb2phy_set_linestate(0, PHY_SET_USB_CONNECT);
+			wait_conn_delay = 100;
+			while (wait_conn_delay != 0) {
+				mdelay(wait_conn_delay);
+				while (retry_cnt != 0) {
+					retry_cnt--;
+					rockchip_usb2phy_set_linestate(0, PHY_SET_USB_DONE);
+					linestate = rockchip_usb2phy_get_linestate();
+					if ((linestate & DP_DM_IS_HIGH)) {
+						rockchip_usb2phy_set_linestate(0, PHY_SET_USB_DP_H_DM_L);
+						mdelay(DELAY_MS_WAIT_SE0);
+					} else {
+						printf("receive se0 at retry %d delay %d\n",
+						       retry_cnt, wait_conn_delay);
+						wait_conn_delay = 0;
+						goto connect;
+					}
+				}
+
+				switch (wait_conn_delay) {
+				case 100:
+					retry_cnt = RETRY_CNT;
+					wait_conn_delay = 105;
+					break;
+				case 105:
+					retry_cnt = RETRY_CNT;
+					wait_conn_delay = 150;
+					break;
+				case 150:
+					retry_cnt = RETRY_CNT;
+					wait_conn_delay = 200;
+					break;
+				case 200:
+					retry_cnt = RETRY_CNT;
+					wait_conn_delay = 300;
+					break;
+				case 300:
+					wait_conn_delay = 0;
+					break;
+				default:
+					wait_conn_delay = 0;
+					break;
+				}
+			}
+			mdelay(DELAY_MS_POLL);
+		}
+	}
+
+connect:
 	while (1) {
 		usb_gadget_handle_interrupts(controller_index);
 
 		rc = fsg_main_thread(NULL);
 		if (rc) {
+			if (soc_is_rk3308bs() && (rc == -ENODEV)) {
+				printf("wait for usb connect timeout\n");
+				force_connect = true;
+				goto force_connect;
+			}
+
 			/* Check I/O error */
 			if (rc == -EIO)
 				printf("\rCheck USB cable connection\n");
diff --git a/common/usb_hub.c b/common/usb_hub.c
index 4a33aa36b621..82b19bdc526d 100644
--- a/common/usb_hub.c
+++ b/common/usb_hub.c
@@ -36,10 +36,12 @@
 #include <asm/state.h>
 #endif
 #include <asm/unaligned.h>
+#include <asm/arch/cpu.h>
 
 DECLARE_GLOBAL_DATA_PTR;
 
 #include <usb.h>
+#include "../drivers/usb/host/ehci.h"
 
 #define USB_BUFSIZ	512
 
@@ -451,12 +453,25 @@ static int usb_scan_port(struct usb_device_scan *usb_scan)
 	if (get_timer(0) < hub->query_delay)
 		return 0;
 
+#if CONFIG_IS_ENABLED(DM_USB) && CONFIG_IS_ENABLED(USB_EHCI_HCD)
+retry:
+#endif
 	ret = usb_get_port_status(dev, i + 1, portsts);
 	if (ret < 0) {
 		debug("get_port_status failed\n");
 		if (get_timer(0) >= hub->connect_timeout) {
 			debug("devnum=%d port=%d: timeout\n",
 			      dev->devnum, i + 1);
+#if CONFIG_IS_ENABLED(DM_USB) && CONFIG_IS_ENABLED(USB_EHCI_HCD)
+			if (soc_is_rk3308bs() &&
+			    usb_hub_is_root_hub(dev->dev) &&
+			    !strcmp(dev->controller_dev->driver->name,
+				    "ehci_generic")) {
+				ret = ehci_hs_reset(dev->controller_dev);
+				if (!ret)
+					goto retry;
+			}
+#endif
 			/* Remove this device from scanning list */
 			list_del(&usb_scan->list);
 			free(usb_scan);
diff --git a/drivers/phy/phy-rockchip-inno-usb2.c b/drivers/phy/phy-rockchip-inno-usb2.c
index 92a918189fbd..d0be2b0fcaf3 100644
--- a/drivers/phy/phy-rockchip-inno-usb2.c
+++ b/drivers/phy/phy-rockchip-inno-usb2.c
@@ -16,6 +16,7 @@
 #include <asm/arch/clock.h>
 #include <asm/arch/cpu.h>
 #include <reset-uclass.h>
+#include <linux/usb/phy-rockchip-usb2.h>
 
 #include "../usb/gadget/dwc2_udc_otg_priv.h"
 
@@ -79,6 +80,9 @@ struct rockchip_chg_det_reg {
 /**
  * struct rockchip_usb2phy_port_cfg: usb-phy port configuration.
  * @phy_sus: phy suspend register.
+ * @phy_pd_en; usb dp/dm 45ohm pulldown enable register.
+ * @phy_fs_xver_en: fs transceiver own and data bit enable register.
+ * @phy_fs_enc_dis: fs transceiver with opmode disable NRZI encoding.
  * @bvalid_det_en: vbus valid rise detection enable register.
  * @bvalid_det_st: vbus valid rise detection status register.
  * @bvalid_det_clr: vbus valid rise detection clear register.
@@ -103,6 +107,9 @@ struct rockchip_chg_det_reg {
  */
 struct rockchip_usb2phy_port_cfg {
 	struct usb2phy_reg	phy_sus;
+	struct usb2phy_reg	phy_pd_en;
+	struct usb2phy_reg	phy_fs_xver_en;
+	struct usb2phy_reg	phy_fs_enc_dis;
 	struct usb2phy_reg	bvalid_det_en;
 	struct usb2phy_reg	bvalid_det_st;
 	struct usb2phy_reg	bvalid_det_clr;
@@ -255,6 +262,32 @@ static bool rockchip_chg_primary_det_retry(struct rockchip_usb2phy *rphy)
 	return vout;
 }
 
+static int rockchip_u2phy_get_vbus(void)
+{
+	const struct rockchip_usb2phy_port_cfg *port_cfg;
+	struct rockchip_usb2phy *rphy;
+	struct udevice *udev;
+	void __iomem *base;
+	int ret;
+
+	ret = uclass_get_device_by_name(UCLASS_PHY, "usb2-phy", &udev);
+	if (ret == -ENODEV) {
+		pr_err("%s: get u2phy node failed: %d\n", __func__, ret);
+		return ret;
+	}
+
+	rphy = dev_get_priv(udev);
+	base = get_reg_base(rphy);
+	port_cfg = &rphy->phy_cfg->port_cfgs[USB2PHY_PORT_OTG];
+
+	if (!property_enabled(base, &port_cfg->utmi_bvalid)) {
+		pr_info("%s: no vbus detect\n", __func__);
+		return POWER_SUPPLY_TYPE_UNKNOWN;
+	}
+
+	return POWER_SUPPLY_TYPE_USB;
+}
+
 int rockchip_chg_get_type(void)
 {
 	const struct rockchip_usb2phy_port_cfg *port_cfg;
@@ -367,7 +400,10 @@ int rockchip_u2phy_vbus_detect(void)
 {
 	int chg_type;
 
-	chg_type = rockchip_chg_get_type();
+	if (soc_is_rk3308bs())
+		chg_type = rockchip_u2phy_get_vbus();
+	else
+		chg_type = rockchip_chg_get_type();
 
 	return (chg_type == POWER_SUPPLY_TYPE_USB ||
 		chg_type == POWER_SUPPLY_TYPE_USB_CDP) ? 1 : 0;
@@ -405,6 +441,77 @@ void otg_phy_init(struct dwc2_udc *dev)
 	mdelay(2);
 }
 
+void rockchip_usb2phy_set_linestate(int phy_id, enum usb_linestate
+				    set_linestate)
+{
+	const struct rockchip_usb2phy_port_cfg *port_cfg;
+	struct rockchip_usb2phy *rphy;
+	struct udevice *udev;
+	void __iomem *base;
+	int ret;
+
+	ret = uclass_get_device_by_name(UCLASS_PHY, "usb2-phy", &udev);
+	if (ret == -ENODEV) {
+		pr_err("%s: get u2phy node failed: %d\n", __func__, ret);
+		return;
+	}
+
+	rphy = dev_get_priv(udev);
+	base = get_reg_base(rphy);
+	port_cfg = &rphy->phy_cfg->port_cfgs[phy_id];
+
+	switch (set_linestate) {
+	case PHY_SET_USB_DP_H_DM_L:
+		property_enable(base, &port_cfg->phy_pd_en, false);
+		if (phy_id == USB2PHY_PORT_OTG)
+			property_enable(base, &port_cfg->phy_fs_xver_en, true);
+		break;
+	case PHY_SET_USB_DISC:
+		property_enable(base, &port_cfg->phy_pd_en, true);
+		if (phy_id == USB2PHY_PORT_OTG)
+			property_enable(base, &port_cfg->phy_fs_xver_en, true);
+		break;
+	case PHY_SET_USB_CONNECT:
+		property_enable(base, &port_cfg->phy_pd_en, false);
+		break;
+	case PHY_SET_USB_DONE:
+		property_enable(base, &port_cfg->phy_pd_en, false);
+		if (phy_id == USB2PHY_PORT_OTG)
+			property_enable(base, &port_cfg->phy_fs_xver_en, false);
+		break;
+	case PHY_SET_USB_FS_ENC_DIS:
+		property_enable(base, &port_cfg->phy_fs_enc_dis, true);
+		break;
+	default:
+		break;
+	}
+}
+
+int rockchip_usb2phy_get_linestate(void)
+{
+	const struct rockchip_usb2phy_port_cfg *port_cfg;
+	struct rockchip_usb2phy *rphy;
+	struct udevice *udev;
+	void __iomem *base;
+	unsigned int ul, ul_mask;
+	int ret;
+
+	ret = uclass_get_device_by_name(UCLASS_PHY, "usb2-phy", &udev);
+	if (ret == -ENODEV) {
+		pr_err("%s: get u2phy node failed: %d\n", __func__, ret);
+		return 0;
+	}
+
+	rphy = dev_get_priv(udev);
+	base = get_reg_base(rphy);
+	port_cfg = &rphy->phy_cfg->port_cfgs[USB2PHY_PORT_OTG];
+
+	ul = readl(base + port_cfg->utmi_ls.offset);
+	ul_mask = GENMASK(port_cfg->utmi_ls.bitend,
+			  port_cfg->utmi_ls.bitstart);
+	return ((ul & ul_mask) >> port_cfg->utmi_ls.bitstart);
+}
+
 static int rockchip_usb2phy_reset(struct rockchip_usb2phy *rphy)
 {
 	int ret;
@@ -580,6 +687,7 @@ static int rockchip_usb2phy_probe(struct udevice *dev)
 		dev_err(dev, "get the base address of usb phy failed\n");
 	}
 
+	printf("\nusb2phy version: V20230526\n");
 	if (!strncmp(parent->name, "root_driver", 11) &&
 	    dev_read_bool(dev, "rockchip,grf")) {
 		ret = uclass_get_device_by_phandle(UCLASS_SYSCON, dev,
@@ -1160,6 +1268,8 @@ static const struct rockchip_usb2phy_cfg rk3308_phy_cfgs[] = {
 		.port_cfgs	= {
 			[USB2PHY_PORT_OTG] = {
 				.phy_sus	= { 0x0100, 8, 0, 0, 0x1d1 },
+				.phy_pd_en	= { 0x0100, 8, 0, 0, 0x003 },
+				.phy_fs_xver_en	= { 0x010c, 11, 0, 0x019, 0x159 },
 				.bvalid_det_en	= { 0x3020, 2, 2, 0, 1 },
 				.bvalid_det_st	= { 0x3024, 2, 2, 0, 1 },
 				.bvalid_det_clr = { 0x3028, 2, 2, 0, 1 },
@@ -1182,6 +1292,8 @@ static const struct rockchip_usb2phy_cfg rk3308_phy_cfgs[] = {
 			},
 			[USB2PHY_PORT_HOST] = {
 				.phy_sus	= { 0x0104, 8, 0, 0, 0x1d1 },
+				.phy_pd_en	= { 0x0104, 8, 0, 0, 0x003 },
+				.phy_fs_enc_dis	= { 0x0104, 8, 0, 0, 0x1db },
 				.ls_det_en	= { 0x3020, 1, 1, 0, 1 },
 				.ls_det_st	= { 0x3024, 1, 1, 0, 1 },
 				.ls_det_clr	= { 0x3028, 1, 1, 0, 1 },
diff --git a/drivers/usb/gadget/dwc2_udc_otg.c b/drivers/usb/gadget/dwc2_udc_otg.c
index 076bd2c08ed1..f0a89c0b11f9 100644
--- a/drivers/usb/gadget/dwc2_udc_otg.c
+++ b/drivers/usb/gadget/dwc2_udc_otg.c
@@ -919,6 +919,20 @@ int dwc2_udc_probe(struct dwc2_plat_otg_data *pdata)
 	return retval;
 }
 
+#define WAIT_USB_CONN_TIMEOUT	(CONFIG_SYS_HZ << 1)
+bool dwc2_usb_gadget_is_connected(void)
+{
+	struct dwc2_udc *dev = the_controller;
+
+	if ((dev->gadget.speed == USB_SPEED_FULL) && (!dev->connected)) {
+		debug("ts %ld\n", get_timer(dev->ts));
+		if (get_timer(dev->ts) > WAIT_USB_CONN_TIMEOUT)
+			return false;
+	}
+
+	return true;
+}
+
 int dwc2_udc_handle_interrupt(void)
 {
 	u32 intr_status = readl(&reg->gintsts);
diff --git a/drivers/usb/gadget/dwc2_udc_otg_priv.h b/drivers/usb/gadget/dwc2_udc_otg_priv.h
index 669d3d0dc6ce..6e0264ea2816 100644
--- a/drivers/usb/gadget/dwc2_udc_otg_priv.h
+++ b/drivers/usb/gadget/dwc2_udc_otg_priv.h
@@ -85,6 +85,7 @@ struct dwc2_udc {
 
 	unsigned req_pending:1, req_std:1;
 	unsigned connected:1;
+	ulong ts;
 };
 
 #define ep_is_in(EP) (((EP)->bEndpointAddress&USB_DIR_IN) == USB_DIR_IN)
diff --git a/drivers/usb/gadget/dwc2_udc_otg_xfer_dma.c b/drivers/usb/gadget/dwc2_udc_otg_xfer_dma.c
index 4fdcd5c56c78..10fbf76c7969 100644
--- a/drivers/usb/gadget/dwc2_udc_otg_xfer_dma.c
+++ b/drivers/usb/gadget/dwc2_udc_otg_xfer_dma.c
@@ -505,6 +505,8 @@ static int dwc2_udc_irq(int irq, void *_dev)
 				usb_status);
 			set_max_pktsize(dev, USB_SPEED_HIGH);
 		}
+
+		dev->ts = get_timer(0);
 	}
 
 	if (intr_status & INT_EARLY_SUSPEND) {
@@ -578,6 +580,7 @@ static int dwc2_udc_irq(int irq, void *_dev)
 					reconfig_usbd(dev);
 
 				dev->ep0state = WAIT_FOR_SETUP;
+				dev->connected = 0;
 				reset_available = 0;
 				dwc2_udc_pre_setup();
 			} else
diff --git a/drivers/usb/gadget/f_mass_storage.c b/drivers/usb/gadget/f_mass_storage.c
index 8f6d055f0767..c642bf665bbe 100644
--- a/drivers/usb/gadget/f_mass_storage.c
+++ b/drivers/usb/gadget/f_mass_storage.c
@@ -240,6 +240,7 @@
 /* #define VERBOSE_DEBUG */
 /* #define DUMP_MSGS */
 
+#include <asm/arch/cpu.h>
 #include <config.h>
 #include <hexdump.h>
 #include <malloc.h>
@@ -674,6 +675,9 @@ static int sleep_thread(struct fsg_common *common)
 			k = 0;
 		}
 
+		if (soc_is_rk3308bs() && (!dwc2_usb_gadget_is_connected()))
+			return -ENODEV;
+
 		usb_gadget_handle_interrupts(0);
 	}
 	common->thread_wakeup_needed = 0;
diff --git a/drivers/usb/host/ehci-hcd.c b/drivers/usb/host/ehci-hcd.c
index ca5d022ed29b..67e651f544e6 100644
--- a/drivers/usb/host/ehci-hcd.c
+++ b/drivers/usb/host/ehci-hcd.c
@@ -20,6 +20,8 @@
 #include <linux/compiler.h>
 
 #include "ehci.h"
+#include <asm/arch/cpu.h>
+#include <linux/usb/phy-rockchip-usb2.h>
 
 #ifndef CONFIG_USB_MAX_CONTROLLER_COUNT
 #define CONFIG_USB_MAX_CONTROLLER_COUNT 1
@@ -756,7 +758,8 @@ static int ehci_submit_root(struct usb_device *dev, unsigned long pipe,
 	case USB_REQ_GET_STATUS | ((USB_RT_PORT | USB_DIR_IN) << 8):
 		memset(tmpbuf, 0, 4);
 		reg = ehci_readl(status_reg);
-		if (reg & EHCI_PS_CS)
+		if (reg & EHCI_PS_CS ||
+		    ctrl->port_connect_status)
 			tmpbuf[0] |= USB_PORT_STAT_CONNECTION;
 		if (reg & EHCI_PS_PE)
 			tmpbuf[0] |= USB_PORT_STAT_ENABLE;
@@ -785,7 +788,8 @@ static int ehci_submit_root(struct usb_device *dev, unsigned long pipe,
 			tmpbuf[1] |= USB_PORT_STAT_HIGH_SPEED >> 8;
 		}
 
-		if (reg & EHCI_PS_CSC)
+		if (reg & EHCI_PS_CSC ||
+		    ctrl->port_connect_status_change)
 			tmpbuf[2] |= USB_PORT_STAT_C_CONNECTION;
 		if (reg & EHCI_PS_PEC)
 			tmpbuf[2] |= USB_PORT_STAT_C_ENABLE;
@@ -815,6 +819,11 @@ static int ehci_submit_root(struct usb_device *dev, unsigned long pipe,
 			if ((reg & (EHCI_PS_PE | EHCI_PS_CS)) == EHCI_PS_CS &&
 			    !ehci_is_TDI() &&
 			    EHCI_PS_IS_LOWSPEED(reg)) {
+#if CONFIG_IS_ENABLED(DM_USB)
+				if (soc_is_rk3308bs() &&
+				    !ehci_hs_reset(usb_get_bus(dev->dev)))
+					break;
+#endif
 				/* Low speed device, give up ownership. */
 				debug("port %d low speed --> companion\n",
 				      port - 1);
@@ -846,6 +855,13 @@ static int ehci_submit_root(struct usb_device *dev, unsigned long pipe,
 					reg = ehci_readl(status_reg);
 					if ((reg & (EHCI_PS_PE | EHCI_PS_CS))
 					    == EHCI_PS_CS && !ehci_is_TDI()) {
+#if CONFIG_IS_ENABLED(DM_USB)
+						if (soc_is_rk3308bs() &&
+						    !ehci_hs_reset(usb_get_bus(dev->dev))) {
+							ctrl->portreset |= 1 << port;
+							break;
+						}
+#endif
 						debug("port %d full speed --> companion\n", port - 1);
 						reg &= ~EHCI_PS_CLEAR;
 						reg |= EHCI_PS_PO;
@@ -889,6 +905,7 @@ static int ehci_submit_root(struct usb_device *dev, unsigned long pipe,
 			break;
 		case USB_PORT_FEAT_C_CONNECTION:
 			reg |= EHCI_PS_CSC;
+			ctrl->port_connect_status_change = 0;
 			break;
 		case USB_PORT_FEAT_OVER_CURRENT:
 			reg |= EHCI_PS_OCC;
@@ -1603,6 +1620,109 @@ static int ehci_get_max_xfer_size(struct udevice *dev, size_t *size)
 	return 0;
 }
 
+int ehci_hs_reset(struct udevice *dev)
+{
+	struct ehci_ctrl *ctrl = dev_get_priv(dev);
+	struct ehci_hcor *hcor = ctrl->hcor;
+	u32 temp, command;
+	u8 retry_count = 10;
+	int ret = -1;
+
+	if (!ctrl)
+		return ret;
+
+reset_retry:
+	command = ehci_readl(&hcor->or_usbcmd);
+
+	/* Force DP high and DM low */
+	temp = ehci_readl(&hcor->or_portsc[0]);
+	if (temp & EHCI_PS_PO)
+		ehci_writel(&hcor->or_portsc[0], temp & ~EHCI_PS_PO);
+	/* Test J_STATE */
+	temp &= ~(0xf << 16);
+	temp |= (0x1 << 16);
+	ehci_writel(&hcor->or_portsc[0], temp);
+	ehci_readl(&hcor->or_portsc[0]);
+
+	rockchip_usb2phy_set_linestate(1, PHY_SET_USB_FS_ENC_DIS);
+	mdelay(10);
+
+	/* ehci halt */
+	ehci_writel(&hcor->or_usbintr, 0);
+	command &= ~CMD_RUN;
+	temp = ehci_readl(&hcor->or_usbcmd);
+	temp &= ~(CMD_RUN | CMD_IAAD);
+	ehci_writel(&hcor->or_usbcmd, temp);
+
+	ret = handshake(&hcor->or_usbsts, STS_HALT, STS_HALT,
+			HCHALT_TIMEOUT);
+	if (ret) {
+		printf("EHCI failed to halt host controller.\n");
+		return ret;
+	}
+
+	/* ehci reset */
+	ret = ehci_reset(ctrl);
+	if (ret)
+		return ret;
+
+	ehci_writel(&hcor->or_usbcmd, command);
+	ehci_writel(&hcor->or_configflag, FLAG_CF);
+	ehci_readl(&hcor->or_usbcmd); /* unblock posted writes */
+
+	/* re-init operational registers */
+	ehci_writel(&hcor->or_ctrldssegment, 0);
+	ehci_writel(&ctrl->hcor->or_periodiclistbase,
+		    (unsigned long)ctrl->periodic_list);
+	ehci_writel(&ctrl->hcor->or_asynclistaddr,
+		    virt_to_phys(&ctrl->qh_list));
+
+	/* restore CMD_RUN */
+	command |= CMD_RUN;
+	ehci_writel(&hcor->or_usbcmd, command);
+
+	temp = ehci_readl(&hcor->or_portsc[0]);
+	ehci_writel(&hcor->or_portsc[0], temp | EHCI_PS_PP);
+
+	rockchip_usb2phy_set_linestate(1, PHY_SET_USB_DONE);
+
+	/* Put port in reset */
+	temp = ehci_readl(&hcor->or_portsc[0]);
+	ehci_writel(&hcor->or_portsc[0], temp | EHCI_PS_PR);
+	mdelay(50);
+	/* Terminate the reset */
+	ehci_writel(&hcor->or_portsc[0], temp & ~EHCI_PS_PR);
+
+	/*
+	 * A host controller must terminate the reset
+	 * and stabilize the state of the port within
+	 * 2 milliseconds
+	 */
+	ret = handshake(&hcor->or_portsc[0], EHCI_PS_PR, 0,
+			2 * 1000);
+	if (ret) {
+		printf("EHCI port reset error\n");
+		return ret;
+	}
+
+	/* Try to wait for HS device attached */
+	ret = handshake(&hcor->or_portsc[0], EHCI_PS_PE, 0x4,
+			10 * 1000 /* 50ms */);
+	if (ret) {
+		debug("no device is present\n");
+		ctrl->port_connect_status_change = 0;
+		ctrl->port_connect_status = 0;
+		if (--retry_count > 0)
+			goto reset_retry;
+	} else {
+		debug("hs device is present\n");
+		ctrl->port_connect_status_change = 1;
+		ctrl->port_connect_status = 1;
+	}
+
+	return ret;
+}
+
 int ehci_register(struct udevice *dev, struct ehci_hccr *hccr,
 		  struct ehci_hcor *hcor, const struct ehci_ops *ops,
 		  uint tweaks, enum usb_init_type init)
@@ -1623,6 +1743,8 @@ int ehci_register(struct udevice *dev, struct ehci_hccr *hccr,
 	ctrl->hccr = hccr;
 	ctrl->hcor = hcor;
 	ctrl->priv = ctrl;
+	ctrl->port_connect_status_change = 0;
+	ctrl->port_connect_status = 0;
 
 	ctrl->init = init;
 	if (ctrl->init == USB_INIT_DEVICE)
diff --git a/drivers/usb/host/ehci.h b/drivers/usb/host/ehci.h
index 98a6b4a9089c..0d05eb93987d 100644
--- a/drivers/usb/host/ehci.h
+++ b/drivers/usb/host/ehci.h
@@ -255,6 +255,8 @@ struct ehci_ctrl {
 	struct ehci_ops ops;
 	struct usb_hub_descriptor hub;
 	void *priv;	/* client's private data */
+	u8 port_connect_status_change;
+	u8 port_connect_status;
 };
 
 /**
@@ -289,6 +291,9 @@ int ehci_register(struct udevice *dev, struct ehci_hccr *hccr,
 		  struct ehci_hcor *hcor, const struct ehci_ops *ops,
 		  uint tweaks, enum usb_init_type init);
 int ehci_deregister(struct udevice *dev);
+#if CONFIG_IS_ENABLED(DM_USB)
+int ehci_hs_reset(struct udevice *dev);
+#endif
 extern struct dm_usb_ops ehci_usb_ops;
 
 /* EHCI PHY functions */
diff --git a/include/linux/usb/gadget.h b/include/linux/usb/gadget.h
index 705ed1497640..efaff9ad117d 100644
--- a/include/linux/usb/gadget.h
+++ b/include/linux/usb/gadget.h
@@ -963,6 +963,8 @@ extern void usb_ep_autoconfig_reset(struct usb_gadget *);
 
 extern int usb_gadget_handle_interrupts(int index);
 
+extern bool dwc2_usb_gadget_is_connected(void);
+
 #if CONFIG_IS_ENABLED(DM_USB_GADGET)
 int usb_gadget_initialize(int index);
 int usb_gadget_release(int index);
diff --git a/include/linux/usb/phy-rockchip-usb2.h b/include/linux/usb/phy-rockchip-usb2.h
index 01eeb17f732a..0e73cdb3e768 100644
--- a/include/linux/usb/phy-rockchip-usb2.h
+++ b/include/linux/usb/phy-rockchip-usb2.h
@@ -7,6 +7,15 @@
 #ifndef _PHY_ROCKCHIP_USB2_H
 #define _PHY_ROCKCHIP_USB2_H
 
+enum usb_linestate {
+	PHY_SET_USB_NONE,
+	PHY_SET_USB_DP_H_DM_L,
+	PHY_SET_USB_DISC,
+	PHY_SET_USB_CONNECT,
+	PHY_SET_USB_DONE,
+	PHY_SET_USB_FS_ENC_DIS,
+};
+
 extern int rockchip_chg_get_type(void);
 
 #if defined(CONFIG_PHY_ROCKCHIP_INNO_USB2) ||\
@@ -14,11 +23,23 @@ extern int rockchip_chg_get_type(void);
     defined(CONFIG_PHY_ROCKCHIP_NANENG_USB2)
 
 int rockchip_u2phy_vbus_detect(void);
+void rockchip_usb2phy_set_linestate(int phy_id,
+				    enum usb_linestate set_linestate);
+int rockchip_usb2phy_get_linestate(void);
 #else
 static inline int rockchip_u2phy_vbus_detect(void)
 {
 	return -ENOSYS;
 }
+
+static inline void rockchip_usb2phy_set_linestate(int phy_id,
+			enum usb_linestate set_linestate)
+{}
+
+static inline int rockchip_usb2phy_get_linestate(void)
+{
+	return 0;
+}
 #endif
 
 #endif /* _PHY_ROCKCHIP_USB2_H */
-- 
2.17.1

