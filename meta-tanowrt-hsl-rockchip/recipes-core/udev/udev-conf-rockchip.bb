#
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2022, Rockchip Electronics Co., Ltd
# Copyright (c) 2022-2024, Tano Systems LLC
#

DESCRIPTION = "Rockchip configuration files for udev"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PV = "1.0.0"
PR = "rk1"

SRC_URI = " \
	file://99-rockchip-permissions.rules \
"

SRC_URI:append:nanopi-r5c = " \
	file://70-nanopi-r5c-eth.rules \
"

S = "${WORKDIR}"

do_install() {
	install -d ${D}${sysconfdir}/udev/rules.d
	install -m 0644 ${WORKDIR}/99-rockchip-permissions.rules ${D}${sysconfdir}/udev/rules.d/99-rockchip-permissions.rules
}

do_install:append:nanopi-r5c() {
	install -d ${D}${sysconfdir}/udev/rules.d
	install -m 0644 ${WORKDIR}/70-nanopi-r5c-eth.rules ${D}${sysconfdir}/udev/rules.d/70-nanopi-r5c-eth.rules
}
