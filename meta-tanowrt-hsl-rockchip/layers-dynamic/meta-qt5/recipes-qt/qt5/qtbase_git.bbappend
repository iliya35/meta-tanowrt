# Copyright (C) 2019, Fuzhou Rockchip Electronics Co., Ltd
# Copyright (c) 2024, Tano Systems LLC. All rights reserved.
# Released under the MIT license (see COPYING.MIT for the terms)

PR:append = ".rk0"

PACKAGECONFIG_GL = "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'gles2', '', d)}"
PACKAGECONFIG_FONTS = "fontconfig"

PACKAGECONFIG:append = " gbm kms eglfs linuxfb examples "
PACKAGECONFIG:remove = "tests"
