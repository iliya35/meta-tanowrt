#
# SPDX-License-Identifier: MIT
# Copyright (c) 2024 Tano Systems LLC. All rights reserved.
#
PR:append = ".rk1"
SRC_URI:remove = "file://0001-Fix-QtTestBrowser-build-without-OpenGL.patch"

# Enable OpenGL
OECMAKE_CXX_FLAGS:remove = "-DQT_NO_OPENGL"

# Disable/enable features
EXTRA_OECMAKE:remove = "-DENABLE_OPENGL=OFF"
EXTRA_OECMAKE:append = "\
	-DENABLE_OPENGL=ON \
"
