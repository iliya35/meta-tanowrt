#
# SPDX-License-Identifier: MIT
# Copyright (c) 2024 Tano Systems LLC. All rights reserved.
#
PR:append:boardcon-em356x = ".rk0"
PR:append:nanopi-r5c = ".rk0"

COMPATIBLE_MACHINE = "boardcon-em356x|nanopi-r5c"

# Use sw-description file from Rockchip HSL twimg-default-swu.bb recipe
FILESEXTRAPATHS:prepend := "${TANOWRT_HSL_BASE}/recipes-image/images/twimg-default-swu:"
