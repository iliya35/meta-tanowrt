#
# SPDX-License-Identifier: MIT
# Copyright (c) 2024 Tano Systems LLC. All rights reserved.
#

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files:"

SRC_URI += "file://chromium.init"

do_install:append() {
	# Remove init script for sysvinit
	rm -f ${D}${sysconfdir}/init.d/chromium-init.sh

	# Add init script for procd
	install -m 0755 ${WORKDIR}/chromium.init ${D}${sysconfdir}/init.d/chromium
}

RDEPENDS:${PN} += "weston"

inherit tanowrt-services

TANOWRT_SERVICE_PACKAGES = "chromium-ozone-wayland"
TANOWRT_SERVICE_SCRIPTS_chromium-ozone-wayland += "chromium"
TANOWRT_SERVICE_STATE_chromium-ozone-wayland-chromium ?= "enabled"
