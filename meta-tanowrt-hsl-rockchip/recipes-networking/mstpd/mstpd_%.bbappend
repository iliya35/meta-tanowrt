#
# SPDX-License-Identifier: MIT
# Copyright (c) 2022, 2024 Tano Systems LLC. All rights reserved.
#
PR:append:boardcon-em3568 = ".rk0"
PR:append:nanopi-r5c = ".rk0"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files:"
