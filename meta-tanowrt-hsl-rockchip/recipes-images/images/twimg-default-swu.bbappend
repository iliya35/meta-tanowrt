#
# SPDX-License-Identifier: MIT
# Copyright (c) 2022, 2024 Tano Systems LLC. All rights reserved.
#
PR:append = ".rk0"

COMPATIBLE_MACHINE = "boardcon-em356x|rock-pi-s|nanopi-r5c"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/:"
