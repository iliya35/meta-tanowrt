From 35dc48ca6c009b108e369b7e9bd5b501eb92a4ee Mon Sep 17 00:00:00 2001
From: William Wu <william.wu@rock-chips.com>
Date: Tue, 20 Jun 2023 13:30:03 +0300
Subject: [PATCH] rk3308bs: usb: dwc2: host: fix abnormal linestate

Signed-off-by: William Wu <william.wu@rock-chips.com>
Change-Id: I8861c64bffb2bc8563ec865d9251ec3b2810880b
[Update for kernel 4.19]
Signed-off-by: Anton Kikin <a.kikin@tano-systems.com>
---
 drivers/usb/core/usb.c  |   2 +-
 drivers/usb/dwc2/core.h |   3 ++
 drivers/usb/dwc2/hcd.c  | 113 +++++++++++++++++++++++++++++++++++++++-
 include/linux/usb/hcd.h |   3 ++
 4 files changed, 119 insertions(+), 2 deletions(-)

diff --git a/drivers/usb/core/usb.c b/drivers/usb/core/usb.c
index 35f632165c8f..36319932a464 100644
--- a/drivers/usb/core/usb.c
+++ b/drivers/usb/core/usb.c
@@ -65,7 +65,7 @@ int usb_disabled(void)
 EXPORT_SYMBOL_GPL(usb_disabled);
 
 #ifdef	CONFIG_PM
-static int usb_autosuspend_delay = 2;		/* Default delay value,
+static int usb_autosuspend_delay = -1;		/* Default delay value,
 						 * in seconds */
 module_param_named(autosuspend, usb_autosuspend_delay, int, 0644);
 MODULE_PARM_DESC(autosuspend, "default autosuspend delay");
diff --git a/drivers/usb/dwc2/core.h b/drivers/usb/dwc2/core.h
index f41011fdfeb6..1edd2486c94b 100644
--- a/drivers/usb/dwc2/core.h
+++ b/drivers/usb/dwc2/core.h
@@ -954,6 +954,8 @@ struct dwc2_hregs_backup {
  * @status_buf_dma:     DMA address for status_buf
  * @start_work:         Delayed work for handling host A-cable connection
  * @reset_work:         Delayed work for handling a port reset
+ * @hs_reset_work:      Delayed work for handling force DP high and DM low and
+ *                      do a port reset at the same time similar to hs reset.
  * @otg_port:           OTG port number
  * @frame_list:         Frame list
  * @frame_list_dma:     Frame list DMA address
@@ -1134,6 +1136,7 @@ struct dwc2_hsotg {
 
 	struct delayed_work start_work;
 	struct delayed_work reset_work;
+	struct delayed_work hs_reset_work;
 	u8 otg_port;
 	u32 *frame_list;
 	dma_addr_t frame_list_dma;
diff --git a/drivers/usb/dwc2/hcd.c b/drivers/usb/dwc2/hcd.c
index 0254b3626484..ecfe2a98b18d 100644
--- a/drivers/usb/dwc2/hcd.c
+++ b/drivers/usb/dwc2/hcd.c
@@ -52,9 +52,11 @@
 
 #include <linux/usb/hcd.h>
 #include <linux/usb/ch11.h>
+#include <linux/rockchip/cpu.h>
 
 #include "core.h"
 #include "hcd.h"
+#include "../core/hub.h"
 
 static void dwc2_port_resume(struct dwc2_hsotg *hsotg);
 
@@ -3586,6 +3588,7 @@ static int dwc2_hcd_hub_control(struct dwc2_hsotg *hsotg, u16 typereq,
 				u16 wvalue, u16 windex, char *buf, u16 wlength)
 {
 	struct usb_hub_descriptor *hub_desc;
+	struct usb_hcd *hcd = dwc2_hsotg_to_hcd(hsotg);
 	int retval = 0;
 	u32 hprt0;
 	u32 port_status;
@@ -3827,7 +3830,8 @@ static int dwc2_hcd_hub_control(struct dwc2_hsotg *hsotg, u16 typereq,
 		if (wvalue != USB_PORT_FEAT_TEST && (!windex || windex > 1))
 			goto error;
 
-		if (!hsotg->flags.b.port_connect_status) {
+		if (!hsotg->flags.b.port_connect_status &&
+		    (!HCD_POWER_ON(hcd))) {
 			/*
 			 * The port is disconnected, which means the core is
 			 * either in device mode or it soon will be. Just
@@ -3857,6 +3861,7 @@ static int dwc2_hcd_hub_control(struct dwc2_hsotg *hsotg, u16 typereq,
 			pwr = hprt0 & HPRT0_PWR;
 			hprt0 |= HPRT0_PWR;
 			dwc2_writel(hsotg, hprt0, HPRT0);
+			clear_bit(HCD_FLAG_POWER_ON, &hcd->flags);
 			if (!pwr)
 				dwc2_vbus_supply_init(hsotg);
 			break;
@@ -4417,6 +4422,79 @@ static void dwc2_hcd_reset_func(struct work_struct *work)
 	spin_unlock_irqrestore(&hsotg->lock, flags);
 }
 
+/*
+ * High speed reset work queue function
+ */
+static void _dwc2_hcd_hs_reset_work(struct work_struct *work)
+{
+	struct dwc2_hsotg *hsotg = container_of(work, struct dwc2_hsotg,
+						hs_reset_work.work);
+	struct usb_device *rhdev;
+	struct usb_hub *hub;
+	struct usb_hcd *hcd;
+	u32 hprt0, speed;
+	int i;
+
+	dev_dbg(hsotg->dev, "%s enter\n", __func__);
+
+	if (dwc2_is_device_mode(hsotg))
+		return;
+
+	hcd = dwc2_hsotg_to_hcd(hsotg);
+	if (!hcd) {
+		dev_err(hsotg->dev, "%s: hcd is NULL\n", __func__);
+		return;
+	}
+
+	rhdev = hcd->self.root_hub;
+	hub = usb_hub_to_struct_hub(rhdev);
+
+	for (i = 0; i < rhdev->maxchild; i++) {
+		if ((hub->ports[i]->child) &&
+		    ((hub->ports[i]->child->state) >= USB_STATE_CONFIGURED))
+			return;
+	}
+
+	printk_once(KERN_WARNING "%s start hs handshake\n", __func__);
+	dwc2_disable_host_interrupts(hsotg);
+
+	/* Force both DP and DM low to trigger disconnect */
+	phy_set_linestate(hsotg->phy, PHY_SET_USB_DISC);
+	msleep(10);
+
+	/* Force DP high and DM low likes high speed connecting */
+	phy_set_linestate(hsotg->phy, PHY_SET_USB_DP_H_DM_L);
+	msleep(10);
+
+	/* Put port in rest */
+	hprt0 = dwc2_read_hprt0(hsotg);
+	hprt0 &= ~HPRT0_SUSP;
+	hprt0 |= HPRT0_PWR | HPRT0_RST;
+	dwc2_writel(hsotg, hprt0, HPRT0);
+
+	/* Clear reset bit in 50ms (HS) */
+	msleep(50);
+	hprt0 &= ~HPRT0_RST;
+	dwc2_writel(hsotg, hprt0, HPRT0);
+
+	/* Check the device attached or not */
+	hprt0 = dwc2_read_hprt0(hsotg);
+	dev_dbg(hsotg->dev, "after reset, hprt0=%08x\n", hprt0);
+	speed = (hprt0 & HPRT0_SPD_MASK) >> HPRT0_SPD_SHIFT;
+
+	if ((hprt0 & HPRT0_CONNSTS) && (speed == HPRT0_SPD_HIGH_SPEED)) {
+		dev_dbg(hsotg->dev, "hs device is present\n");
+		dwc2_hcd_connect(hsotg);
+	} else {
+		dev_dbg(hsotg->dev, "no device is present\n");
+	}
+
+	/* Reset is done, disable software to set DP and DM voltage */
+	phy_set_linestate(hsotg->phy, PHY_SET_USB_DONE);
+
+	dwc2_enable_host_interrupts(hsotg);
+}
+
 /*
  * =========================================================================
  *  Linux HC Driver Functions
@@ -5004,6 +5082,8 @@ static irqreturn_t _dwc2_hcd_irq(struct usb_hcd *hcd)
 	return dwc2_handle_hcd_intr(hsotg);
 }
 
+#define DWC2_HS_RESET_SCHEDULE_DELAY	(5 * HZ)
+
 /*
  * Creates Status Change bitmap for the root hub and root port. The bitmap is
  * returned in buf. Bit 0 is the status change indicator for the root hub. Bit 1
@@ -5013,6 +5093,32 @@ static irqreturn_t _dwc2_hcd_irq(struct usb_hcd *hcd)
 static int _dwc2_hcd_hub_status_data(struct usb_hcd *hcd, char *buf)
 {
 	struct dwc2_hsotg *hsotg = dwc2_hcd_to_hsotg(hcd);
+	struct usb_device *rhdev;
+	struct usb_hub *hub;
+	int linestate, i;
+	bool sch_work = true;
+
+	if (dwc2_is_host_mode(hsotg)) {
+		linestate = phy_get_linestate(hsotg->phy);
+		printk_once(KERN_WARNING "dwc2 hcd linestate 0x%02x\n",
+			    linestate);
+
+		if (soc_is_rk3308bs() && ((linestate & 0x3) != 0)) {
+			rhdev = hcd->self.root_hub;
+			hub = usb_hub_to_struct_hub(rhdev);
+
+			for (i = 0; i < rhdev->maxchild; i++) {
+				if ((hub->ports[i]->child) &&
+				    ((hub->ports[i]->child->state) >=
+					USB_STATE_CONFIGURED))
+					sch_work = false;
+			}
+
+			if (sch_work)
+				schedule_delayed_work(&hsotg->hs_reset_work,
+					      DWC2_HS_RESET_SCHEDULE_DELAY);
+		}
+	}
 
 	buf[0] = dwc2_hcd_is_status_changed(hsotg, 1) << 1;
 	return buf[0] != 0;
@@ -5194,6 +5300,8 @@ static void dwc2_hcd_free(struct dwc2_hsotg *hsotg)
 		destroy_workqueue(hsotg->wq_otg);
 	}
 
+	cancel_delayed_work_sync(&hsotg->hs_reset_work);
+
 	del_timer(&hsotg->wkp_timer);
 }
 
@@ -5345,6 +5453,9 @@ int dwc2_hcd_init(struct dwc2_hsotg *hsotg)
 	/* Initialize port reset work */
 	INIT_DELAYED_WORK(&hsotg->reset_work, dwc2_hcd_reset_func);
 
+	/* Initialize high speed reset work */
+	INIT_DELAYED_WORK(&hsotg->hs_reset_work, _dwc2_hcd_hs_reset_work);
+
 	/*
 	 * Allocate space for storing data on status transactions. Normally no
 	 * data is sent, but this space acts as a bit bucket. This must be
diff --git a/include/linux/usb/hcd.h b/include/linux/usb/hcd.h
index e34faee4ee7d..84b46e1591c7 100644
--- a/include/linux/usb/hcd.h
+++ b/include/linux/usb/hcd.h
@@ -119,6 +119,7 @@ struct usb_hcd {
 #define HCD_FLAG_DEAD			6	/* controller has died? */
 #define HCD_FLAG_INTF_AUTHORIZED	7	/* authorize interfaces? */
 #define HCD_FLAG_DEV_AUTHORIZED		8	/* authorize devices? */
+#define HCD_FLAG_POWER_ON		9	/* power on */
 
 	/* The flags can be tested using these macros; they are likely to
 	 * be slightly faster than test_bit().
@@ -146,6 +147,8 @@ struct usb_hcd {
 #define HCD_DEV_AUTHORIZED(hcd) \
 	((hcd)->flags & (1U << HCD_FLAG_DEV_AUTHORIZED))
 
+#define HCD_POWER_ON(hcd)	((hcd)->flags & (1U << HCD_FLAG_POWER_ON))
+
 	/* Flags that get set only during HCD registration or removal. */
 	unsigned		rh_registered:1;/* is root hub registered? */
 	unsigned		rh_pollable:1;	/* may we poll the root hub? */
-- 
2.17.1

