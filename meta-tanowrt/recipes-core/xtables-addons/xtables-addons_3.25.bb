#
# SPDX-License-Identifier: MIT
# Copyright (c) 2024 Tano Systems LLC. All rights reserved.
#

PV = "3.25"
PR = "tano0.${INC_PR}"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches-3.x:${THISDIR}/${PN}/patches-3.25:${THISDIR}/${PN}/files:"

SRC_URI = "\
	https://inai.de/files/xtables-addons/${BP}.tar.xz \
	file://001-fix-kernel-version-detection.patch \
	file://100-add-rtsp-conntrack.patch \
	file://200-add-lua-packetscript.patch \
	file://201-fix-lua-packetscript.patch \
	file://202-add-lua-autoconf.patch \
	file://400-fix-IFF_LOWER_UP-musl.patch \
	file://0001-Unset-LDFLAGS-for-kernel-modules.patch \
"

SRC_URI[sha256sum] = "8c9f4c6a8e92eb7cfbf03f4ebcb1e1e793256c2efd0226d83312bfb0ffe14b84"

S = "${WORKDIR}/xtables-addons-${PV}"

do_compile:prepend() {
	# install additional empty wrappers
	touch ${S}/extensions/LUA/lua/include/stdarg.h
	touch ${S}/extensions/LUA/lua/include/stddef.h
}

require xtables-addons.inc
