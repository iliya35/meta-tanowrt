#
# SPDX-License-Identifier: MIT
#
# This file Copyright (C) 2015 Khem Raj <raj.khem@gmail.com> and
#           Copyright (C) 2018 Daniel Dickinson <cshored@thecshore.com>
#           Copyright (C) 2019-2021 Anton Kikin <a.kikin@tano-systems.com>
#

# 24.01.2024
# base-files: move uci_set_poe() to uci-defaults.sh
OPENWRT_SRCREV = "6f83a708c8f1df14da9a24609d32bd7263d1798b"
OPENWRT_BRANCH = "master"

INC_PR = "21"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = " file://${WORKDIR}/openwrt/LICENSES/GPL-2.0;md5=fbfe04ebfede83e097ba08389d089069"

SRC_URI += "\
	git://${GIT_OPENWRT_ORG}/openwrt/openwrt.git;name=openwrt;destsuffix=openwrt;branch=${OPENWRT_BRANCH};protocol=https \
"

SRCREV_openwrt = "${OPENWRT_SRCREV}"
