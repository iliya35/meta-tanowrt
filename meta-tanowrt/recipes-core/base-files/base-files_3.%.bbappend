#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2015 Khem Raj <raj.khem@gmail.com>
# Copyright (C) 2018 Daniel Dickinson <cshored@thecshore.com>
# Copyright (C) 2018-2024 Anton Kikin <a.kikin@tano-systems.com>
#

PR:append = ".tano98.${INC_PR}"

RDEPENDS:${PN} += "tano-version"
RDEPENDS:${PN} += "board-d"

# Initial timezone
OPENWRT_ZONENAME ?= "Europe/Moscow"
OPENWRT_TIMEZONE ?= "MSK-3"

# System hostname
OPENWRT_HOSTNAME ?= "tanowrt"

# Initial hwclock parameters
OPENWRT_HWCLOCK_DEV       ?= "/dev/rtc0"
OPENWRT_HWCLOCK_LOCALTIME ?= "1"

# Enable/disable overlay partition resize at preinit stage
TANOWRT_ENABLE_OVERLAY_RESIZE ?= "0"

TANOWRT_OVERLAY_RESIZE_LIMIT_SECTORS ?= "0"
TANOWRT_OVERLAY_RESIZE_LIMIT_SECTORS[doc] = "Limit overlay partition size in sectors (0 - no limit)"

SUMMARY = "Base files from openembedded and openwrt projects"
HOMEPAGE = "http://wiki.openwrt.org/"

RRECOMMENDS:${PN} += "tzdata"
RRECOMMENDS:${PN} += "${@oe.utils.conditional('TANOWRT_ENABLE_OVERLAY_RESIZE', '1', \
	'gptfdisk parted e2fsprogs-resize2fs', '', d)}"

require base-files.inc

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files-arch:"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

ARCH_X86 = "i586 x86_64"

PREINIT_SCRIPTS = "\
	file://preinit/50_partitions_setup \
	file://preinit/80_mount_root \
	file://preinit/81_init_banner \
	${@oe.utils.conditional('TANOWRT_ENABLE_OVERLAY_RESIZE', '1', 'file://preinit/51_overlay_resize', '', d)} \
"

PREINIT_SCTIPTS_X86 = "\
	file://preinit/01_sysinfo \
	file://preinit/02_load_x86_ucode \
	file://preinit/15_essential_fs_x86 \
	file://preinit/20_check_iso \
	file://preinit/79_move_config \
"

PREINIT_SCRIPTS:append:qemuarm64 = " file://preinit/01_sysinfo "
PREINIT_SCRIPTS:append:qemux86 = " ${PREINIT_SCTIPTS_X86}"
PREINIT_SCRIPTS:append:qemux86-64 = " ${PREINIT_SCTIPTS_X86}"


SRC_URI:append = "\
    ${PREINIT_SCRIPTS} \
    file://shells \
    file://sysctl.conf \
    file://issue \
    file://hostname \
    file://boot.init \
    file://system.init \
    file://sysfixtime.init \
    file://hotplug-call \
    file://sysctl.d/10-default.conf \
    file://sysctl.d/20-noswap.conf \
    file://wifi \
    file://config_generate \
    file://board_detect \
"

# Only for x86 and x86-64 architectures
FILESEXTRAPATHS:prepend:qemux86 := "${THISDIR}/${PN}/files-arch/x86:"
FILESEXTRAPATHS:prepend:qemux86-64 := "${THISDIR}/${PN}/files-arch/x86:"

SG = "${WORKDIR}/openwrt"
STMP = "${WORKDIR}/stmp"

inherit tanowrt-services

TANOWRT_SERVICE_PACKAGES = "base-files"

TANOWRT_SERVICE_SCRIPTS_base-files += "boot done sysctl umount gpio_switch led sysfixtime system"
TANOWRT_SERVICE_STATE_base-files-gpio_switch ?= "enabled"
TANOWRT_SERVICE_STATE_base-files-led ?= "enabled"
TANOWRT_SERVICE_STATE_base-files-sysfixtime ?= "enabled"
TANOWRT_SERVICE_STATE_base-files-system ?= "enabled"
TANOWRT_SERVICE_STATE_base-files-boot ?= "enabled"
TANOWRT_SERVICE_STATE_base-files-done ?= "enabled"
TANOWRT_SERVICE_STATE_base-files-sysctl ?= "enabled"
TANOWRT_SERVICE_STATE_base-files-umount ?= "enabled"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

BASEFILESISSUEINSTALL ?= "do_install_basefilesissue"

do_install[vardeps] += "OPENWRT_ZONENAME OPENWRT_TIMEZONE OPENWRT_VERSION_SED"

OPENWRT_CONFIG_TARGET_PREINIT_SUPPRESS_STDERR ?= "y"
OPENWRT_CONFIG_TARGET_PREINIT_TIMEOUT ?= "2"
OPENWRT_CONFIG_TARGET_PREINIT_INIT_PATH ?= "/usr/sbin:/usr/bin:/sbin:/bin"
OPENWRT_CONFIG_TARGET_INIT_ENV ?= ""
OPENWRT_CONFIG_TARGET_INIT_CMD ?= "/sbin/init"
OPENWRT_CONFIG_TARGET_INIT_SUPPRESS_STDERR ?= "y"
OPENWRT_CONFIG_TARGET_PREINIT_IFNAME ?= ""
OPENWRT_CONFIG_TARGET_PREINIT_IP ?= "192.168.1.1"
OPENWRT_CONFIG_TARGET_PREINIT_NETMASK ?= "255.255.255.0"
OPENWRT_CONFIG_TARGET_PREINIT_BROADCAST ?= "192.168.1.255"
OPENWRT_CONFIG_TARGET_PREINIT_SHOW_NETMSG ?= ""
OPENWRT_CONFIG_TARGET_PREINIT_SUPPRESS_FAILSAFE_NETMSG ?= "y"
OPENWRT_CONFIG_TARGET_PREINIT_DISABLE_FAILSAFE ?= ""

do_install:append () {
	rm -rf ${D}${localstatedir}/backups
	rm -rf ${D}${localstatedir}/local

	# We need to munge openwrt base-files before copying
	# Some file come from regular OE base-files and others
	# belong in other recipes, or are not applicable
	rm -rf ${STMP}
	mkdir -p ${STMP}
	cp -dR --preserve=mode,links ${SG}/package/base-files/files/* ${STMP}/

	# procd - earlyinit
	rm -f ${STMP}/etc/inittab

	# From OE base-files or netbase
	rm -f ${STMP}/etc/hosts
	rm -f ${STMP}/etc/rpc
	rm -f ${STMP}/etc/services
	rm -f ${STMP}/etc/protocols
	rm -f ${STMP}/etc/banner
	ln -sf /etc/issue ${STMP}/etc/banner

	# Not applicable to OE flavour
	rm -f ${STMP}/etc/uci-defaults/10_migrate-shadow
	rm -f ${STMP}/etc/uci-defaults/11_migrate-sysctl

	# These depend on mechanisms not in OE build process
	rm -f ${STMP}/etc/uci-defaults/13_fix_group_user

	# In base-files-scripts-openwrt
	rm -f ${STMP}/lib/functions.sh
	rm -f ${STMP}/lib/functions/uci-defaults.sh
	rm -f ${STMP}/lib/functions/system.sh
	rm -f ${STMP}/bin/ipcalc.sh

	# In base-files-scripts-sysupgrade
	rm -f ${STMP}/etc/sysupgrade.conf
	rm -f ${STMP}/sbin/sysupgrade
	rm -rf ${STMP}/lib/upgrade
	rm -f ${STMP}/sbin/firstboot

	# Moved to tano-version.bb
	rm -f ${STMP}/etc/os-release
	rm -f ${STMP}/usr/lib/os-release

	# Copy what is applicable to rootfs
	cp -dR --preserve=mode,links ${STMP}/* ${D}
	rm -rf ${STMP}

	mkdir -p ${D}${base_sbindir}
	mkdir -p ${D}${sysconfdir}/rc.d

	# FIXME: Should this change for OE?
	mkdir -p ${D}/overlay

	ln -sf /tmp/resolv.conf /tmp/TZ ${D}${sysconfdir}/
	ln -sf /proc/mounts ${D}${sysconfdir}/mtab

	chmod 0600 ${D}${sysconfdir}/shadow
	chmod 1777 ${D}/tmp

	sed -i "s#%PATH%#/usr/sbin:/sbin:/usr/bin:/bin#g" \
		${D}${sysconfdir}/profile

	if [ "${@bb.utils.contains('COMBINED_FEATURES', 'wifi', '1', '0', d)}" = "1" ]; then
		# For netifd package
		install -d -m 0755 ${WORKDIR}/wifi ${D}${base_sbindir}/
	fi

	install -d -m 0755 ${D}${sysconfdir}/config
	install -d -m 0755 ${D}${sysconfdir}/init.d
	install -d -m 0755 ${D}${sysconfdir}/sysctl.d

	install -m 0644 ${WORKDIR}/sysctl.conf ${D}${sysconfdir}/sysctl.conf
	install -m 0644 ${WORKDIR}/sysctl.d/10-default.conf ${D}${sysconfdir}/sysctl.d/10-default.conf
	install -m 0644 ${WORKDIR}/sysctl.d/20-noswap.conf ${D}${sysconfdir}/sysctl.d/20-noswap.conf

	# these files generated in tanowrt-image-buildinfo.bbclass
	rm -f ${D}${sysconfdir}/device_info
	rm -f ${D}${sysconfdir}/openwrt_release
	rm -f ${D}${sysconfdir}/openwrt_version

	install -m 0644 ${WORKDIR}/issue ${D}${sysconfdir}/issue
	install -m 0644 ${WORKDIR}/hostname ${D}${sysconfdir}/hostname
	install -m 0755 ${WORKDIR}/system.init ${D}${sysconfdir}/init.d/system
	install -m 0755 ${WORKDIR}/boot.init ${D}${sysconfdir}/init.d/boot
	install -m 0755 ${WORKDIR}/sysfixtime.init ${D}${sysconfdir}/init.d/sysfixtime
	install -m 0755 ${WORKDIR}/hotplug-call ${D}${base_sbindir}/hotplug-call
	install -m 0644 ${WORKDIR}/shells ${D}${sysconfdir}/shells

	install -m 0755 ${WORKDIR}/config_generate ${D}${base_bindir}/config_generate
	install -m 0755 ${WORKDIR}/board_detect ${D}${base_bindir}/board_detect

	install -m 0644 ${WORKDIR}/profile ${D}${sysconfdir}/profile

	if [ -n "${PREINIT_SCRIPTS}" ]; then
		install -d ${D}${TANOWRT_PATH_PREINIT}
		for f in ${PREINIT_SCRIPTS}; do
			FILEPATH="${f#file://}"
			install -m 0644 ${WORKDIR}/${FILEPATH} ${D}${TANOWRT_PATH_PREINIT}/
		done
	fi

	if [ "${TANOWRT_ENABLE_OVERLAY_RESIZE}" = "1" ]; then
		sed -i "s/%TANOWRT_OVERLAY_RESIZE_LIMIT_SECTORS%/${TANOWRT_OVERLAY_RESIZE_LIMIT_SECTORS}/g" \
			${D}${TANOWRT_PATH_PREINIT}/51_overlay_resize
	fi

	rm ${D}${sysconfdir}/issue.net
	rm ${D}${sysconfdir}/TZ

	# Generate config defaults for config_generate
	install -d ${D}${nonarch_base_libdir}/config-defaults
	{
		# System hostname
		echo "SYSTEM_HOSTNAME='${OPENWRT_HOSTNAME}'"

		# Timezone and zonename
		echo "SYSTEM_TIMEZONE='${OPENWRT_TIMEZONE}'"
		echo "SYSTEM_ZONENAME='${OPENWRT_ZONENAME}'"

		# Default hwclock parameters
		echo "SYSTEM_HWCLOCK_DEV='${OPENWRT_HWCLOCK_DEV}'"
		echo "SYSTEM_HWCLOCK_LOCALTIME='${OPENWRT_HWCLOCK_LOCALTIME}'"

	} > ${D}${nonarch_base_libdir}/config-defaults/00-system

	echo "${OPENWRT_HOSTNAME}" > ${D}${sysconfdir}/hostname

	rm -rf ${D}/var/run
	rm -rf ${D}/run
	ln -s /var/run ${D}/run

	rm -rf ${D}/proc/mounts
	rm -rf ${D}${sysconfdir}/motd
	rm -rf ${D}${sysconfdir}/skel
	rm -rf ${D}${sysconfdir}/filesystems

	if [ "${ROOT_HOME}" != "/home/root" ]; then
		# Make symlink /home/root to real root's home
		mkdir -p ${D}/home
		ln -s ${ROOT_HOME} ${D}/home/root
	fi

	install -dm 0755 ${D}${libdir}/locale

	# Common default PATH
	install -dm 0755 ${D}${TANOWRT_PATH_PREINIT}
	PREINIT_CONF="${D}${TANOWRT_PATH_PREINIT}/00_preinit.conf"
	echo "pi_suppress_stderr=\"${OPENWRT_CONFIG_TARGET_PREINIT_SUPPRESS_STDERR}\"" >${PREINIT_CONF}
	echo "fs_failsafe_wait_timeout=${OPENWRT_CONFIG_TARGET_PREINIT_TIMEOUT}" >>${PREINIT_CONF}
	echo "pi_init_path=\"${OPENWRT_CONFIG_TARGET_PREINIT_INIT_PATH}\"" >>${PREINIT_CONF}
	echo "pi_init_env=\"${OPENWRT_CONFIG_TARGET_INIT_ENV}\"" >>${PREINIT_CONF}
	echo "pi_init_cmd=\"${OPENWRT_CONFIG_TARGET_INIT_CMD}\"" >>${PREINIT_CONF}
	echo "pi_init_suppress_stderr=\"${OPENWRT_CONFIG_TARGET_INIT_SUPPRESS_STDERR}\"" >>${PREINIT_CONF}
	echo "pi_ifname=\"${OPENWRT_CONFIG_TARGET_PREINIT_IFNAME}\"" >>${PREINIT_CONF}
	echo "pi_ip=\"${OPENWRT_CONFIG_TARGET_PREINIT_IP}\"" >>${PREINIT_CONF}
	echo "pi_netmask=\"${OPENWRT_CONFIG_TARGET_PREINIT_NETMASK}\"" >>${PREINIT_CONF}
	echo "pi_broadcast=\"${OPENWRT_CONFIG_TARGET_PREINIT_BROADCAST}\"" >>${PREINIT_CONF}
	echo "pi_preinit_net_messages=\"${OPENWRT_CONFIG_TARGET_PREINIT_SHOW_NETMSG}\"" >>${PREINIT_CONF}
	echo "pi_preinit_no_failsafe_netmsg=\"${OPENWRT_CONFIG_TARGET_PREINIT_SUPPRESS_FAILSAFE_NETMSG}\"" >>${PREINIT_CONF}
	echo "pi_preinit_no_failsafe=\"${OPENWRT_CONFIG_TARGET_PREINIT_DISABLE_FAILSAFE}\"" >>${PREINIT_CONF}

	sed -i "s#%PATH%#/usr/sbin:/usr/bin:/sbin:/bin#g" \
		${D}${sysconfdir}/preinit \
		${D}${base_sbindir}/hotplug-call

	sed -i 's#:/root:#:${ROOT_HOME}:#' ${D}${sysconfdir}/passwd
}

do_install:append:qemuarm64() {
	install -dm 0755 ${D}${TANOWRT_PATH_PREINIT}
	install -m 0644 ${WORKDIR}/preinit/01_sysinfo ${D}${TANOWRT_PATH_PREINIT}/01_sysinfo
}

pkg_preinst:${PN} () {
    :
}

pkg_postinst:${PN}:append() {
	rm -rf $D/var/lock
	mkdir -p $D/var/lock
}

FILES:${PN} = "/"

RDEPENDS:${PN} += "\
	${PN}-scripts-openwrt \
	${PN}-scripts-sysupgrade \
	getrandom \
	factory-reset \
"

RSUGGESTS:${PN} += "\
	procd \
	ubox \
"

PACKAGE_ARCH = "${MACHINE_ARCH}"

CONFFILES:${PN}:append = "\
	${sysconfdir}/resolv.conf \
	${sysconfdir}/nsswitch.conf \
	${sysconfdir}/host.conf \
	${sysconfdir}/shells \
	${sysconfdir}/config/system \
	${sysconfdir}/fstab \
	${sysconfdir}/sysctl.conf \
	${sysconfdir}/shadow \
	${sysconfdir}/hostname \
	${sysconfdir}/group \
	${sysconfdir}/profile \
	${sysconfdir}/shinit \
	${sysconfdir}/passwd \
	${sysconfdir}/sysctl.d/10-default.conf \
	${sysconfdir}/sysctl.d/20-noswap.conf \
	${sysconfdir}/rc.local \
"
