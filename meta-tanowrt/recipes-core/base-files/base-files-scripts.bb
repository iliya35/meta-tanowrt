#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2018 Daniel Dickinson <cshored@thecshore.com>
# Copyright (C) 2018-2019, 2022, 2024 Anton Kikin <a.kikin@tano-systems.com>
#

require base-files.inc

PR = "tano9.${INC_PR}"

DESCRIPTION = "Subpackages from base-files from OpenWrt core"
HOMEPAGE = "http://wiki.openwrt.org/"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

REQUIRED_DISTRO_FEATURES = "procd"
CONFLICT_DISTRO_FEATURES = "sysvinit systemd"
inherit features_check

SC = "${WORKDIR}/openwrt/package/base-files/files"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

PACKAGES = "\
	${PN}-openwrt \
	${PN}-sysupgrade \
"

# sysupgrade
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files-sysupgrade:${THISDIR}/${PN}/files:"
SRC_URI += "\
	file://etc/sysupgrade.conf \
	file://lib/upgrade/common.sh \
	file://lib/upgrade/do_stage2 \
	file://lib/upgrade/fwtool.sh \
	file://lib/upgrade/keep.d \
	file://lib/upgrade/nand.sh \
	file://lib/upgrade/emmc.sh \
	file://lib/upgrade/legacy-sdcard.sh \
	file://lib/upgrade/stage2 \
	file://lib/upgrade/keep.d/base-files-essential \
	file://sbin/firstboot \
	file://sbin/sysupgrade \
	file://lib/functions/uci-defaults.sh \
"

do_install:append () {
	install -dm 0755 ${D}${sysconfdir}/config

	install -Dm 0644 ${SC}/lib/functions.sh ${D}${nonarch_base_libdir}/functions.sh
	install -Dm 0644 ${SC}/lib/functions/system.sh ${D}${nonarch_base_libdir}/functions/system.sh
	install -Dm 0755 ${SC}/bin/ipcalc.sh ${D}${base_bindir}/ipcalc.sh

	# sysupgrade
	install -Dm 0644 ${WORKDIR}/etc/sysupgrade.conf ${D}${sysconfdir}/sysupgrade.conf
	install -Dm 0755 ${WORKDIR}/sbin/sysupgrade ${D}${base_sbindir}/sysupgrade
	install -Dm 0755 ${WORKDIR}/sbin/firstboot ${D}${base_sbindir}/firstboot

	install -dm 0755 ${D}/lib/upgrade
	install -m 0755 ${WORKDIR}/lib/upgrade/do_stage2 ${D}${nonarch_base_libdir}/upgrade
	install -m 0755 ${WORKDIR}/lib/upgrade/stage2 ${D}${nonarch_base_libdir}/upgrade
	install -m 0644 ${WORKDIR}/lib/upgrade/common.sh ${D}${nonarch_base_libdir}/upgrade
	install -m 0644 ${WORKDIR}/lib/upgrade/fwtool.sh ${D}${nonarch_base_libdir}/upgrade
	install -m 0644 ${WORKDIR}/lib/upgrade/nand.sh ${D}${nonarch_base_libdir}/upgrade
	install -m 0644 ${WORKDIR}/lib/upgrade/emmc.sh ${D}${nonarch_base_libdir}/upgrade
	install -m 0644 ${WORKDIR}/lib/upgrade/legacy-sdcard.sh ${D}${nonarch_base_libdir}/upgrade

	install -dm 0755 ${D}/lib/upgrade/keep.d
	install -m 0644 ${WORKDIR}/lib/upgrade/keep.d/base-files-essential ${D}${nonarch_base_libdir}/upgrade/keep.d

	install -dm 0755 ${D}/lib/functions
	install -m 0644 ${WORKDIR}/lib/functions/uci-defaults.sh ${D}${nonarch_base_libdir}/functions/uci-defaults.sh
}

FILES:${PN}-openwrt = "\
                      ${nonarch_base_libdir}/functions.sh \
                      ${nonarch_base_libdir}/functions/uci-defaults.sh \
                      ${nonarch_base_libdir}/functions/system.sh \
                      ${base_bindir}/ipcalc.sh \
                      ${sysconfdir}/config \
                      "

FILES:${PN}-sysupgrade = "\
                         ${sysconfdir}/sysupgrade.conf \
                         ${base_sbindir}/sysupgrade \
                         ${nonarch_base_libdir}/upgrade/ \
                         ${base_sbindir}/firstboot \
                         "

CONFFILES:${PN}-openwrt += "\
                           ${sysconfdir}/config \
                           "

CONFFILES:${PN}-sysupgrade += "\
                              ${sysconfdir}/sysupgrade.conf \
                              "

PACKAGE_ARCH = "${MACHINE_ARCH}"

BBCLASSEXTEND = "native"
