#
# SPDX-License-Identifier: MIT
# Copyright (c) 2018-2023 Tano Systems LLC. All rights reserved.
#

require conf/distro/include/tanowrt.inc

# Linguas / locales
IMAGE_LINGUAS ?= "ru-ru.utf-8"
GLIBC_GENERATE_LOCALES ?= "ru_RU.UTF-8"
ENABLE_BINARY_LOCALE_GENERATION ?= "1"
LOCALE_UTF8_ONLY ?= "0"
LOCALE_UTF8_IS_DEFAULT ?= "0"
LOCALE_UTF8_IS_DEFAULT:class-nativesdk ?= "1"

LUCI_LANGUAGES ?= "en ru"
LUCI_INITIAL_LANG ?= "auto"

# Name and version of the distribution
DISTRO_NAME     = "TanoWrt"
DISTRO_VERSION  = "Kirkstone"
DISTRO          = "tanowrt"
DISTRO_CODENAME = "tanowrt"

# OpenWrt version
OPENWRT_VERSION_MANUFACTURER     ?= "Tano Systems LLC"
OPENWRT_VERSION_MANUFACTURER_URL ?= "${TANO_SYSTEMS_HOMEPAGE}"
OPENWRT_VERSION_HOME_URL         ?= "${TANO_SYSTEMS_HOMEPAGE}"
OPENWRT_VERSION_BUG_URL          ?= "${TANOWRT_ISSUES}"
OPENWRT_VERSION_SUPPORT_URL      ?= "${TANOWRT_ISSUES}"

# Initial timezone
OPENWRT_ZONENAME ?= "Europe/Moscow"
OPENWRT_TIMEZONE ?= "MSK-3"

# LuCI configuration
TANOWRT_LUCI_ENABLE ?= "1"

# Distribution and LuCI version for LuCI
# This data is displayed as Firmware Version in LuCI
LUCI_DISTNAME    ?= "${DISTRO_NAME}"
LUCI_DISTVERSION ?= "${DISTRO_VERSION}"
LUCI_NAME        ?= "LuCI"

# Initial mediaurlbase
LUCI_INITIAL_MEDIAURLBASE ?= "/luci-static/tano"
