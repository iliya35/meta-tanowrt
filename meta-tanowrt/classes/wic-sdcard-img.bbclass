#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Tano Systems LLC. All rights reserved.
#
# A simple class to rename the '.wic' extension to '.sdcard.img' for WIC images
#

inherit wic-img
WIC_IMAGE_CUSTOM_EXTENSION = "sdcard.img"
