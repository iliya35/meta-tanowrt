#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2018, 2020, 2024 Anton Kikin <a.kikin@tano-systems.com>
#

IMAGE_TYPES += "tn-iso"
IMAGE_TYPES_MASKED += "tn-iso"
DEPLOYABLE_IMAGE_TYPES += "tn-iso"
