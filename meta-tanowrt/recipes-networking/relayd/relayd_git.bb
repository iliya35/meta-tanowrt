#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2016 Khem Raj <raj.khem@gmail.com>
# Copyright (C) 2018-2019, 2023 Anton Kikin <a.kikin@tano-systems.com>
#

PR = "tano4"

DESCRIPTION = "Transparent routing / relay daemon"
HOMEPAGE = "http://git.openwrt.org/?p=project/relayd.git;a=summary"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://main.c;beginline=1;endline=17;md5=86aad799085683e0a2e1c2684a20bab2"
SECTION = "base"
DEPENDS += "libubox"

RRECOMMENDS:${PN} += "${@oe.utils.conditional('TANOWRT_LUCI_ENABLE', '1', 'luci-proto-relay', '', d)}"

SRC_URI = "git://${GIT_OPENWRT_ORG}/project/relayd.git;branch=master;protocol=https \
          "

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

SRCREV = "f646ba40489371e69f624f2dee2fc4e19ceec00e"

S = "${WORKDIR}/git"

inherit cmake pkgconfig

FILES:${PN}  += "${libdir}/*"

SRC_URI += "\
	file://relayd.init \
"

inherit tanowrt-services

TANOWRT_SERVICE_PACKAGES = "relayd"
TANOWRT_SERVICE_SCRIPTS_relayd += "relayd"
TANOWRT_SERVICE_STATE_relayd-relayd ?= "enabled"

do_install:append() {
	install -dm 0755 ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/relayd.init ${D}${sysconfdir}/init.d/relayd
}
