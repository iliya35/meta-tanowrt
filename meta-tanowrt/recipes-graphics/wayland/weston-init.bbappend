#
# SPDX-License-Identifier: MIT
# Copyright (c) 2024 Tano Systems LLC. All rights reserved.
#
PR:append = ".tano0"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

inherit tanowrt-services

TANOWRT_SERVICE_PACKAGES = "weston-init"
TANOWRT_SERVICE_SCRIPTS_weston-init += "weston"
TANOWRT_SERVICE_STATE_weston-init-weston ?= "enabled"

INHIBIT_UPDATERCD_BBCLASS = "${@oe.utils.conditional('VIRTUAL-RUNTIME_init_manager', 'procd', '1', '', d)}"
