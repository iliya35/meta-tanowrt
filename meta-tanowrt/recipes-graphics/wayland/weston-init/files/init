#!/bin/sh /etc/rc.common
# Copyright (C) 2024 Tano Systems LLC

START=95
STOP=01

USE_PROCD=1

WESTON_USER=weston
WESTON_GROUP=
WESTON_BIN=/usr/bin/weston
WESTON_LOG=/var/log/weston.log
WESTON_PID=/var/run/weston.pid

start_service() {
	read CMDLINE < /proc/cmdline
	for x in $CMDLINE; do
		case $x in
			weston=false)
				return
				;;
		esac
	done

	if test -e /etc/default/weston; then
		. /etc/default/weston
	fi

	WESTON_XDG_RUNTIME_DIR=/run/user/`id -u ${WESTON_USER}`
	if ! test -d "${WESTON_XDG_RUNTIME_DIR}"; then
		mkdir --parents ${WESTON_XDG_RUNTIME_DIR}
		chmod 0700 ${WESTON_XDG_RUNTIME_DIR}
	fi

	if [ -n "$WESTON_USER" ]; then
		if [ -z "$WESTON_GROUP" ]; then
			# no explicit WESTON_GROUP given, therefore use WESTON_USER
			export WESTON_GROUP="${WESTON_USER}"
		fi
	fi

	if [ -n "${WESTON_USER}" ]; then
		touch ${WESTON_LOG}
		chown ${WESTON_USER}:${WESTON_GROUP} ${WESTON_LOG}
		chown ${WESTON_USER}:${WESTON_GROUP} ${WESTON_XDG_RUNTIME_DIR}
	fi

	procd_open_instance
	procd_set_param command ${WESTON_BIN}
	procd_append_param command --tty="1"
	procd_append_param command --log=${WESTON_LOG}
	procd_append_param command ${OPTARGS}
	procd_set_param env XDG_RUNTIME_DIR=${WESTON_XDG_RUNTIME_DIR}
	procd_set_param pidfile ${WESTON_PID}


	if [ -n "${WESTON_USER}" ]; then
		procd_set_param user ${WESTON_USER}
		procd_set_param group ${WESTON_GROUP}
	fi

	procd_close_instance
}
