From 2bd860388690285854683ab311dc43de039350a1 Mon Sep 17 00:00:00 2001
From: Anton Kikin <a.kikin@tano-systems.com>
Date: Sat, 23 Sep 2023 14:34:51 +0300
Subject: [PATCH 1/2] Do not hardcode RX/TX buffer sizes

Signed-off-by: Anton Kikin <a.kikin@tano-systems.com>
---
 ch347.h     |  3 +++
 mfd-ch347.c | 39 ++++++++++++++++++++++++++++-----------
 spi-ch347.c | 51 +++++++++++++++++++++++++++++++++++++--------------
 3 files changed, 68 insertions(+), 25 deletions(-)

diff --git a/ch347.h b/ch347.h
index 9a0cfe8..c937f0d 100644
--- a/ch347.h
+++ b/ch347.h
@@ -12,4 +12,7 @@ int ch347_xfer(struct platform_device *pdev, const uint8_t *obuf, unsigned obuf_
 
 ch347_mode_t ch347_mode(struct platform_device *pdev);
 
+unsigned int ch347_rx_size(struct platform_device *pdev);
+unsigned int ch347_tx_size(struct platform_device *pdev);
+
 #endif
diff --git a/mfd-ch347.c b/mfd-ch347.c
index 04449c4..39bcf74 100644
--- a/mfd-ch347.c
+++ b/mfd-ch347.c
@@ -25,11 +25,8 @@
 
 #define CH347_PACKET_LENGTH   510
 
-#define CH347_RX_BUFFERS      4
-#define CH347_RX_BUFFER_SIZE  CH347_PACKET_LENGTH
-
-#define CH347_TX_BUFFERS      8
-#define CH347_TX_BUFFER_SIZE  4096
+#define CH347_RX_BUFFERS      16
+#define CH347_TX_BUFFERS      16
 
 #define CH347_USB_VENDOR 0x1a86
 #define CH347_USB_DEVICE_M1 0x55db
@@ -80,6 +77,9 @@ struct ch347_dev {
 	spinlock_t txb_lock;
 	struct semaphore tx_limit_sem;
 
+	unsigned int tx_size;
+	unsigned int rx_size;
+
 	int errors;
 	spinlock_t err_lock;
 
@@ -167,7 +167,7 @@ static int ch347_init_buffers(struct ch347_dev *ch347)
 
 	for (i = 0; i < CH347_RX_BUFFERS; ++i) {
 		ch347->rxb[i].ch347 = ch347;
-		ch347->rxb[i].urb = ch347_urb_alloc(ch347, CH347_RX_BUFFER_SIZE);
+		ch347->rxb[i].urb = ch347_urb_alloc(ch347, ch347->rx_size);
 		init_completion(&ch347->rxb[i].complete);
 
 		if (!ch347->rxb[i].urb) {
@@ -180,7 +180,7 @@ static int ch347_init_buffers(struct ch347_dev *ch347)
 
 	for (i = 0; i < CH347_TX_BUFFERS; ++i) {
 		ch347->txb[i].ch347 = ch347;
-		ch347->txb[i].urb = ch347_urb_alloc(ch347, CH347_TX_BUFFER_SIZE);
+		ch347->txb[i].urb = ch347_urb_alloc(ch347, ch347->tx_size);
 		if (!ch347->txb[i].urb) {
 			retval = -ENOMEM;
 			goto out_free;
@@ -366,12 +366,12 @@ static int ch347_data_xfer(
 	struct ch347_tx_buffer *txb = NULL;
 	int retval = 0;
 
-	if (ibuf && ((ibuf_len > CH347_RX_BUFFER_SIZE) || (ibuf_len == 0))) {
+	if (ibuf && ((ibuf_len > ch347->rx_size) || (ibuf_len == 0))) {
 		retval = -EINVAL;
 		goto exit;
 	}
 
-	if (obuf && ((obuf_len > CH347_TX_BUFFER_SIZE) || (obuf_len == 0))) {
+	if (obuf && ((obuf_len > ch347->tx_size) || (obuf_len == 0))) {
 		retval = -EINVAL;
 		goto exit;
 	}
@@ -533,6 +533,20 @@ ch347_mode_t ch347_mode(struct platform_device *pdev)
 }
 EXPORT_SYMBOL(ch347_mode);
 
+unsigned int ch347_rx_size(struct platform_device *pdev)
+{
+	struct ch347_dev *ch347 = dev_get_drvdata(pdev->dev.parent);
+	return ch347->rx_size;
+}
+EXPORT_SYMBOL(ch347_rx_size);
+
+unsigned int ch347_tx_size(struct platform_device *pdev)
+{
+	struct ch347_dev *ch347 = dev_get_drvdata(pdev->dev.parent);
+	return ch347->tx_size;
+}
+EXPORT_SYMBOL(ch347_tx_size);
+
 /*
  * Sometimes, for some completely unknown and magical reasons,
  * when the driver is loaded for the first time after system
@@ -557,12 +571,12 @@ static int ch347_fixup_startup(struct ch347_dev *ch347) {
 	obuf[2] = 0;
 
 	/* Try to read maximum allowed buffer */
-	ibuf = kzalloc(CH347_RX_BUFFER_SIZE, GFP_KERNEL);
+	ibuf = kzalloc(ch347->rx_size, GFP_KERNEL);
 	if (!ibuf)
 		return -ENOMEM;
 
 	ret = ch347_data_xfer(ch347, obuf, 11,
-		ibuf, CH347_RX_BUFFER_SIZE,
+		ibuf, ch347->rx_size,
 		RXB_FLAG_NO_RESUBMIT, 250);
 
 	if (ret == 11) {
@@ -648,6 +662,9 @@ static int ch347_probe(struct usb_interface *interface, const struct usb_device_
 	ch347->usb_dev = usb_get_dev(interface_to_usbdev(interface));
 	ch347->interface = interface;
 
+	ch347->tx_size = min(usb_endpoint_maxp(epout), CH347_PACKET_LENGTH);
+	ch347->rx_size = min(usb_endpoint_maxp(epin), CH347_PACKET_LENGTH);
+
 	mutex_init(&ch347->io_mutex);
 	sema_init(&ch347->tx_limit_sem, CH347_TX_BUFFERS);
 	init_usb_anchor(&ch347->submitted);
diff --git a/spi-ch347.c b/spi-ch347.c
index 90fed81..10cb064 100644
--- a/spi-ch347.c
+++ b/spi-ch347.c
@@ -27,9 +27,6 @@
 #define CH347_CS_CHANGE		0x80
 #define CH347_CS_IGNORE		0x00
 
-#define IO_BUF_SIZE_RX 510
-#define IO_BUF_SIZE_TX 4096
-
 #define DEFAULT_NUM_CS  2
 
 static int num_cs = DEFAULT_NUM_CS;
@@ -38,8 +35,6 @@ MODULE_PARM_DESC(num_cs, "CS num: (1 or 2)");
 
 const unsigned MAX_SPI_SPEED = 60*1000*1000; //HZ
 const unsigned MIN_SPI_SPEED = MAX_SPI_SPEED >> 7;
-const unsigned MAX_XFER_RX = IO_BUF_SIZE_RX - 3;
-const unsigned MAX_XFER_TX = IO_BUF_SIZE_TX - 3;
 
 enum CH347_SPI_DIR {
 	SPI_DIR_2LINES_FULLDUPLEX = 0, // default
@@ -90,8 +85,11 @@ struct ch347_spi {
 
 	struct mutex io_mutex;
 
-	u8 ibuf[IO_BUF_SIZE_RX];
-	u8 obuf[IO_BUF_SIZE_TX];
+	unsigned int max_xfer_tx;
+	unsigned int max_xfer_rx;
+
+	u8 *ibuf;
+	u8 *obuf;
 
 	u32 speed;
 	u8 mode;
@@ -174,7 +172,7 @@ static int ch347_spi_write(struct ch347_spi *ch347, const u8 *tx_data, u16 data_
 	unsigned len, remaining = data_len, offset;
 	ch347->obuf[0] = CH347_CMD_SPI_OUT;
 	do {
-		len = remaining > MAX_XFER_TX ? MAX_XFER_TX : remaining;
+		len = remaining > ch347->max_xfer_tx ? ch347->max_xfer_tx : remaining;
 		offset = data_len - remaining;
 		ch347->obuf[1] = len;
 		ch347->obuf[2] = len >> 8;
@@ -198,7 +196,7 @@ static int ch347_spi_read(struct ch347_spi *ch347, u8 *rx_data, u32 data_len)
 	ch347->obuf[2] = 0;
 
 	do {
-		len = remaining > MAX_XFER_RX ? MAX_XFER_RX : remaining;
+		len = remaining > ch347->max_xfer_rx ? ch347->max_xfer_rx : remaining;
 		offset = data_len - remaining;
 		memcpy(ch347->obuf + 3, &len, 4);
 		rv = ch347_xfer(ch347->pdev, ch347->obuf, 7, ch347->ibuf, len + 3);
@@ -228,7 +226,7 @@ static int ch347_rdwr(struct ch347_spi *ch347, const u8 *tx_data, u8 *rx_data, u
 		return ch347_spi_write(ch347, tx_data, data_len);
 	}
 	do {
-		len = remaining > MAX_XFER_RX ? MAX_XFER_RX : remaining;
+		len = remaining > ch347->max_xfer_rx ? ch347->max_xfer_rx : remaining;
 		offset = data_len - remaining;
 		ch347->obuf[0] = CH347_CMD_SPI_OUT_IN;
 		ch347->obuf[1] = len;
@@ -440,10 +438,25 @@ static int ch347_spi_probe(struct platform_device *pdev)
 
 	mutex_init(&ch347->io_mutex);
 
+	ch347->max_xfer_tx = ch347_tx_size(pdev) - 3;
+	ch347->max_xfer_rx = ch347_rx_size(pdev) - 3;
+
+	ch347->obuf = kzalloc(ch347->max_xfer_tx, GFP_KERNEL);
+	if (!ch347->obuf) {
+		rv = -ENOMEM;
+		goto free;
+	}
+
+	ch347->ibuf = kzalloc(ch347->max_xfer_rx, GFP_KERNEL);
+	if (!ch347->ibuf) {
+		rv = -ENOMEM;
+		goto free;
+	}
+
 	rv = ch347_get_hw_config(ch347);
 	if (rv < 0) {
 		dev_err(dev, "%s: Failed to get SPI configuration: %d", __func__, rv);
-		return rv;
+		goto free;
 	}
 
 	if (num_cs != 1 && num_cs != 2) {
@@ -464,24 +477,34 @@ static int ch347_spi_probe(struct platform_device *pdev)
 
 	rv = devm_spi_register_master(dev, master);
 	if (rv < 0)
-		return rv;
+		goto free;
 	rv = device_create_file(&master->dev, &dev_attr_new_device);
 	if (rv) {
 		dev_err(dev, "%s: Can not create 'new_device' file: %d", __func__, rv);
-		return rv;
+		goto free;
 	}
 	rv = device_create_file(&master->dev, &dev_attr_delete_device);
 	if (rv) {
 		dev_err(dev, "%s: Can not create 'delete_device' file: %d", __func__, rv);
-		return rv;
+		goto free;
 	}
 
 	return 0;
+
+free:
+	kfree(ch347->obuf);
+	kfree(ch347->ibuf);
+	return rv;
 }
 
 static int ch347_spi_remove(struct platform_device *pdev)
 {
 	struct spi_master *master = platform_get_drvdata(pdev);
+	struct ch347_spi *ch347 = spi_master_get_devdata(master);
+
+	kfree(ch347->ibuf);
+	kfree(ch347->obuf);
+
 	device_remove_file(&master->dev, &dev_attr_new_device);
 	device_remove_file(&master->dev, &dev_attr_delete_device);
 	return 0;
-- 
2.34.1.windows.1

