#
# SPDX-License-Identifier: MIT
# Copyright (C) 2023, Tano Systems LLC. All rights reserved.
#

SUMMARY = "W-CH CH34x/CH910x Linux Serial Driver"
DESCRIPTION = "USB serial driver for USB to UART(s) chip ch342/ch343/ch344/ch347/ch9101/ch9102/ch9103/ch9104, etc."
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

PV = "1.0.0+git${SRCPV}"
PR = "tano1"

SRC_URI = "git://github.com/WCHSoftGroup/ch343ser_linux.git;branch=main;protocol=https;subpath=driver;destsuffix=git/"
SRCREV = "bf1ae33c95b2013e6e881870e771d44df96d2051"
S = "${WORKDIR}/git"

SRC_URI += "file://0001-Update-Makefile-to-be-compatible-for-OE-build.patch;pnum=2"

inherit tanowrt-kernel-module

do_configure[depends] += "virtual/kernel:do_shared_workdir"

RPROVIDES:${PN} += "kernel-module-ch343"
