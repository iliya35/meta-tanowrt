#
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2024, Tano Systems LLC
#

DESCRIPTION = "Board configuration files"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PV = "1.0.0"
PR = "tano1"

BOARD_D_SCRIPTS = ""

SRC_URI = "${BOARD_D_SCRIPTS}"

S = "${WORKDIR}"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

INHIBIT_DEFAULT_DEPS = "1"
ALLOW_EMPTY:${PN} = "1"
PACKAGE_ARCH = "${MACHINE_ARCH}"

do_install() {
	if [ -n "${BOARD_D_SCRIPTS}" ]; then
		install -d ${D}${sysconfdir}/board.d
		for f in ${BOARD_D_SCRIPTS}; do
			FILEPATH="${f#file://}"
			install -m 0755 ${WORKDIR}/${FILEPATH} ${D}${sysconfdir}/board.d/
		done
	fi
}
