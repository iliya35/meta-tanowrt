#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020-2024 Tano Systems LLC. All rights reserved.
#

PR:append = ".tano1"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

PACKAGECONFIG:remove = "sensord"

SRC_URI += "file://sensors.upgrade"

do_install:append() {
	install -d ${D}/lib/upgrade/keep.d
	install -m 0644 ${WORKDIR}/sensors.upgrade ${D}/lib/upgrade/keep.d/sensors

	rm -rf ${D}${sysconfdir}/sysconfig
}

FILES:${PN}-libsensors += "/lib/upgrade/keep.d/sensors"
CONFFILES:${PN}-libsensors = "${sysconfdir}/sensors.d/"
