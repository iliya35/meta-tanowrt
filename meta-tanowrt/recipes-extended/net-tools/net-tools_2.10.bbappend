#
# SPDX-License-Identifier: MIT
# Copyright (c) 2024 Tano Systems LLC. All rights reserved.
#
PR:append = ".tano0"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

SRC_URI += "\
	file://0001-ifconfig-Static-cast-IFF_DYNAMIC-to-short.patch \
"
