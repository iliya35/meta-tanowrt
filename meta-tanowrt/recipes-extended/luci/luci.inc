#
# SPDX-License-Identifier: MIT
# Copyright (c) 2018-2022 Tano Systems LLC. All rights reserved.
#
LUCI_GIT_URI      ?= "git://${TANO_SYSTEMS_LUCI_GIT_BASE_URL}/luci.git"
LUCI_GIT_BRANCH   ?= "tano/master"
LUCI_GIT_PROTOCOL ?= "${TANO_SYSTEMS_LUCI_GIT_PROTOCOL}"

# 01.08.2022
LUCI_GIT_SRCREV ?= "fcabf7876d8cde7ce5c733361f906f585f8d03a5"

PR:append = ".135"

# Default distributive and LuCI name and version
LUCI_DISTNAME     ?= "Host System"
LUCI_DISTVERSION  ?= "SDK"
LUCI_NAME         ?= "LuCI"

# Languages to install
LUCI_LANGUAGES ?= "en"

# Initial startup language
LUCI_INITIAL_LANG ?= "auto"

# Initial mediaurlbase setting
LUCI_INITIAL_MEDIAURLBASE ?= "/luci-static/bootstrap"
