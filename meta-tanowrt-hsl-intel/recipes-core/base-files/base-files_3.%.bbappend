#
# SPDX-License-Identifier: MIT
# Copyright (c) 2022-2024 Tano Systems LLC. All rights reserved.
#

PR:append = ".intel2"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files/:"

PREINIT_SCRIPTS_INTEL_X86 = "\
	file://preinit/01_sysinfo \
	file://preinit/02_load_x86_ucode \
	file://preinit/15_essential_fs_x86 \
	file://preinit/20_check_iso \
	file://preinit/79_move_config \
"

PREINIT_SCRIPTS:append:corei7-64-intel-common = "${PREINIT_SCRIPTS_INTEL_X86}"
PREINIT_SCRIPTS:append:core2-32-intel-common = "${PREINIT_SCRIPTS_INTEL_X86}"
PREINIT_SCRIPTS:append:skylake-64-intel-common = "${PREINIT_SCRIPTS_INTEL_X86}"
