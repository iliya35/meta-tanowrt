#
# SPDX-License-Identifier: MIT
# Copyright (c) 2021 Tano Systems LLC. All rights reserved.
#

PR:append = ".baikal0"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/files:"

PREINIT_SCRIPTS:append = " file://preinit/79_mount_boot "
