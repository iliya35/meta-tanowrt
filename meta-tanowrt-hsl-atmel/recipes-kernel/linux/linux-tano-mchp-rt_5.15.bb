#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2024 Tano Systems LLC. All rights reserved.
# Authors: Anton Kikin <a.kikin@tano-systems.com>
#
# Linux kernel with RT-patch for Microchip ARM SoCs (aka AT91)
#

KERNEL_SRC_URI ?= "git://github.com/linux4sam/linux-at91.git"
KERNEL_SRC_BRANCH ?= "linux-5.15-mchp-rt"
KERNEL_SRC_PROTOCOL ?= "https"
KERNEL_SRC_SRCREV ?= "22ed83424bc3b8dd2b52b5c21f8ad95f1568c698"

LINUX_VERSION ?= "5.15.105-rt61"
LINUX_KERNEL_TYPE ?= "rt"

require linux-tano-mchp_5.15.bb

PV = "${LINUX_VERSION}+git${SRCPV}"
SRC_URI:append = " file://rt.cfg "
